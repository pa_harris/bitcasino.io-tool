USE [master]
GO
/****** Object:  Database [bitcasino]    Script Date: 1/7/2021 3:10:25 PM ******/
CREATE DATABASE [bitcasino]
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [bitcasino].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [bitcasino] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [bitcasino] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [bitcasino] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [bitcasino] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [bitcasino] SET ARITHABORT OFF 
GO
ALTER DATABASE [bitcasino] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [bitcasino] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [bitcasino] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [bitcasino] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [bitcasino] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [bitcasino] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [bitcasino] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [bitcasino] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [bitcasino] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [bitcasino] SET  DISABLE_BROKER 
GO
ALTER DATABASE [bitcasino] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [bitcasino] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [bitcasino] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [bitcasino] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [bitcasino] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [bitcasino] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [bitcasino] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [bitcasino] SET RECOVERY FULL 
GO
ALTER DATABASE [bitcasino] SET  MULTI_USER 
GO
ALTER DATABASE [bitcasino] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [bitcasino] SET DB_CHAINING OFF 
GO
ALTER DATABASE [bitcasino] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [bitcasino] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [bitcasino] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'bitcasino', N'ON'
GO
USE [bitcasino]
GO
/****** Object:  User [na]    Script Date: 1/7/2021 3:10:25 PM ******/
CREATE USER [na] FOR LOGIN [na] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [na]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [na]
GO
ALTER ROLE [db_datareader] ADD MEMBER [na]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [na]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 1/7/2021 3:10:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Accs]    Script Date: 1/7/2021 3:10:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accs](
	[Email] [nvarchar](256) NOT NULL,
	[PasswordRaw] [nvarchar](256) NOT NULL,
	[IsUsing] [bit] NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Accs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 1/7/2021 3:10:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 1/7/2021 3:10:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 1/7/2021 3:10:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 1/7/2021 3:10:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 1/7/2021 3:10:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ConnectionSignalR]    Script Date: 1/7/2021 3:10:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConnectionSignalR](
	[UserId] [nvarchar](128) NOT NULL,
	[ConnectionId] [nvarchar](128) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ConnectionSignalR] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[ConnectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserInfo]    Script Date: 1/7/2021 3:10:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserInfo](
	[UserId] [varchar](256) NOT NULL,
	[PasswordUser] [nvarchar](256) NOT NULL,
	[Duration] [tinyint] NULL,
	[ActiveDate] [date] NOT NULL,
	[IsOnline] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_UserInfo] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [RoleNameIndex]    Script Date: 1/7/2021 3:10:25 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 1/7/2021 3:10:25 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 1/7/2021 3:10:25 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_RoleId]    Script Date: 1/7/2021 3:10:25 PM ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 1/7/2021 3:10:25 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UserNameIndex]    Script Date: 1/7/2021 3:10:25 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
/****** Object:  StoredProcedure [dbo].[Proc_Admin_GetListUser]    Script Date: 1/7/2021 3:10:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Proc_Admin_GetListUser]
	@email NVARCHAR(256) = NULL,
	@username NVARCHAR(256) = NULL,
	@tab NVARCHAR(256) = NULL,
	@pageSize INT,
	@pageNum INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SQL NVARCHAR(MAX) = ''
	DECLARE @RoleUserJoin NVARCHAR(MAX) = ''
	DECLARE @RoleUserSelect NVARCHAR(MAX) = ''


	SET @RoleUserJoin = '	JOIN dbo.AspNetUserRoles ur ON ur.UserId = u.Id
								JOIN dbo.AspNetRoles r ON r.Id = ur.RoleId '
	SET @RoleUserSelect = '	r.Name as Role'
	

	SET @SQL = @SQL + ' SELECT u.Id,
						u.UserName, 
						u.Email, 
						u.PhoneNumber,
						i.PasswordUser,
						i.Duration,
						i.ActiveDate, ' + @RoleUserSelect + '

				FROM	dbo.UserInfo i JOIN dbo.AspNetUsers u ON u.Id = i.UserId '
						+ @RoleUserJoin +
				' WHERE 1 = 1 '

	DECLARE @SQLWhere NVARCHAR(MAX) = ''

	IF (@tab = 'admin')
	BEGIN
	    SET @SQLWhere = @SQLWhere + ' And r.Name = ''Admin'' '    
	END
	ELSE IF (@tab = 'manager')
	BEGIN
	    SET @SQLWhere = @SQLWhere + ' And r.Name = ''Manager'' '    
	END
	else if (@tab = '' or @tab is null)
	begin
		SET @SQLWhere = @SQLWhere + ' And (r.Name = ''Admin'' or r.Name = ''Manager'' or r.Name = ''User'') ' 
	end

	IF (@email IS NOT NULL AND @email <> '')
	BEGIN
		SET @SQLWhere = @SQLWhere + ' And u.Email LIKE ''%'' + @email + ''%'' '        
	END

	IF (@username IS NOT NULL AND @username <> '')
	BEGIN
		SET @SQLWhere = @SQLWhere + ' And u.UserName LIKE ''%'' + @username + ''%'' '        
	END

	SET @SQL = 'DECLARE @TotalRows INT = 0
				SELECT  @TotalRows = COUNT(u.Id)
				FROM dbo.UserInfo i JOIN dbo.AspNetUsers u ON u.Id = i.UserId ' 
				+ @RoleUserJoin +
				' WHERE 1 = 1 '
			+ @SQLWhere
			+ @SQL 
			+ @SQLWhere;


	SET @SQL = @SQL + ' ORDER BY i.ActiveDate DESC OFFSET ' + CAST((@pageNum - 1) * @pageSize AS NVARCHAR(10)) + ' ROWS FETCH NEXT '+ CAST(@pageSize AS NVARCHAR(10)) +' ROWS ONLY '

	PRINT @SQL;

	EXEC sp_executesql @SQL, N'@email NVARCHAR(256), @username NVARCHAR(256)', @email, @username;

END


GO
USE [master]
GO
ALTER DATABASE [bitcasino] SET  READ_WRITE 
GO
