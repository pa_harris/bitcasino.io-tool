﻿using bitcasino.io.selenium.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bitcasino.io.selenium.Helpers
{
    public static class constants
    {
        public static string DomainAPIUrl = "http://phoenix-bot-api.wecheck.co";

        public static bool IsCheckServer { get; set; } = true;
        public static string AccessToken { get; set; }
        public static Accs CurrentAccs { get; set; }
        public static UserAccountInfo Account { get; set; }
        public static string ConnectionHubUrl = $"{DomainAPIUrl}/signalr";


        #region Link API
        public static string ApiGetAccs = $"{DomainAPIUrl}/api/Accs/GetAccs";
        public static string ApiGetUserInfo = $"{DomainAPIUrl}/api/User/GetUserInfo";
        public static string ApiPingUser = $"{DomainAPIUrl}/api/User/PingUser";
        #endregion

    }
}
