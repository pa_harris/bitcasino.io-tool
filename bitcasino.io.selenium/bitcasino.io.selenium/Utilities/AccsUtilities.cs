﻿using bitcasino.io.selenium.Helpers;
using bitcasino.io.selenium.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace bitcasino.io.selenium.Utilities
{
    public static class AccsUtilities
    {
        public async static Task<Accs> GetAccs(Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync(constants.ApiGetAccs);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<Accs>(responeContent);
                        constants.CurrentAccs = respone;
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        if (string.IsNullOrEmpty(responeContent))
                        {
                            callback("Hiện tại không tìm được accs! Vui lòng liên hệ quản trị viên để được hỗ trợ!");
                        }
                        else
                        {
                            callback(responeContent);
                        }
                        return null;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }
            return null;
        }
    }
}
