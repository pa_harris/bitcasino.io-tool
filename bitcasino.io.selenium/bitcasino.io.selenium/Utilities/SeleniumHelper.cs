﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bitcasino.io.selenium.Utilities
{
    public static class SeleniumHelper
    {
        public enum ESeleniumElementAction
        {
            Click = 0,
            SendKey
        }
        public static bool DoActionWithElement(IWebElement element,  ESeleniumElementAction action, string content = null)
        {
            try
            {
                switch (action)
                {
                    case ESeleniumElementAction.Click:
                        element.Click();
                        break;
                    case ESeleniumElementAction.SendKey:
                        element.SendKeys(content);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ee)
            {
                return false;
            }

            return true;
        }
        public static bool FindElementAndDoAction(ChromeDriver driver, By by, ESeleniumElementAction action, string content, out string message)
        {
            IWebElement element = null;
            try
            {
                element = driver.FindElement(by);
            }
            catch (Exception ee)
            {
                message = ee.Message;
                return false;
            }
            if (element != null)
            {
                message = $"Found {by} element.";
            }
            else
            {
                message = $"Cannot find {by} element.";
                return false;
            }

            switch (action)
            {
                case ESeleniumElementAction.Click:
                    try
                    {
                        element.Click();
                    }
                    catch (Exception ee)
                    {
                        message = $"{ee.Message}";
                        return false;
                    }
                    break;
                case ESeleniumElementAction.SendKey:
                    try
                    {
                        element.SendKeys(content);
                    }
                    catch (Exception ee)
                    {
                        message = $"{ee.Message}";
                        return false;
                    }
                    break;
                default:
                    break;
            }

            message = $"Done action with {by} element.";
            return true;
        }
        public static IWebElement FindElement(ChromeDriver driver, By by, out string message)
        {
            IWebElement element = null;
            try
            {
                element = driver.FindElement(by);
            }
            catch (Exception ee)
            {
                message = $"{ee.Message}";
                return null;
            }
            if (element == null)
            {
                message = $"Cannot find {by} element.";
                return null;
            }
            message = $"Found {by} element.";
            return element;
        }
    }
}
