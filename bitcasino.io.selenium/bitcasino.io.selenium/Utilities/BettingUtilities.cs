﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace bitcasino.io.selenium.Utilities
{
    public static class BettingUtilities
    {
        public static ETargetUser TargetUser { get; set; } = ETargetUser.Client;

        public static List<BetItem> ListSelection = new List<BetItem>() { new BetItem { Value = "Red", IsSelecting = false }, new BetItem { Value = "Blue", IsSelecting = false } };

        #region Class
        public class BetItem : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs(propertyName));
            }
            private bool _IsSelecting;
            public bool IsSelecting { get => _IsSelecting; set { _IsSelecting = value; OnPropertyChanged(); } }
            private string _Value;
            public string Value { get => _Value; set { _Value = value; OnPropertyChanged(); } }
        }
        public enum ETargetUser
        {
            Admin = 1,
            Client
        }
        #endregion
        public static void GetAccs()
        {

        }

    }
}
