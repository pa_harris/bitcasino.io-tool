﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using bitcasino.io.selenium.Models;
using bitcasino.io.selenium.Helpers;
using Newtonsoft.Json;

namespace bitcasino.io.selenium.Utilities
{
    public static class UserUtilities
    {
        public async static Task<bool> Login(AccountLoginModel acc, Action<string> callback)
        {
            if (string.IsNullOrEmpty(acc.UserName) || string.IsNullOrEmpty(acc.Password))
            {
                callback("Username or password cannot empty!");
                return false;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer");
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                var formContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", acc.UserName),
                    new KeyValuePair<string, string>("password", acc.Password)
                });

                try
                {
                    var response = await http.PostAsync(constants.DomainAPIUrl + "/token", formContent);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<LoginResponse>(responeContent);
                        if (!string.IsNullOrEmpty(respone.access_token))
                        {
                            constants.AccessToken = respone.access_token;
                            return true;
                        }
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ResponseFailed>(responeContent);
                        callback(respone.error_description);
                        return false;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            constants.AccessToken = null;
            return false;
        }
        public async static Task<bool> GetUserInfo(Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync(constants.ApiGetUserInfo);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<UserAccountInfo>(responeContent);

                        if (respone.Duration == null)
                        {
                            return true;
                        }
                        
                        if ((respone.ActiveDate.AddDays(respone.Duration.Value) - DateTime.Now).TotalSeconds <= 0)
                        {
                            callback("Account has expired!");
                            return false;
                        }
                        if (respone != null)
                        {
                            constants.Account = respone;
                        }

                        return true;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        if (string.IsNullOrEmpty(responeContent))
                        {
                            callback("User data not found! Contact to administrator for support!");
                        }
                        else
                        {
                            callback(responeContent);
                        }
                        return false;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return false;
        }
        public async static Task<int> PingUser(Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.PostAsync(constants.ApiPingUser, null);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<int>(responeContent);
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        callback(responeContent);
                        return 0;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }
            callback("Something went wrong! Please comeback later!");
            return 0;
        }
    }
}
