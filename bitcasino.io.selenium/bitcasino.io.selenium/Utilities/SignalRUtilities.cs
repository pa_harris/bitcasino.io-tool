﻿using bitcasino.io.selenium.Helpers;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace bitcasino.io.selenium.Utilities
{
    public static class SignalRUtilities
    {
        public static IHubProxy Proxy { get; set; }
        public static HubConnection Connection { get; set; }

        public static void ConnectSignalR()
        {
            var querystringData = new Dictionary<string, string>();
            querystringData.Add("bearerToken", constants.AccessToken);
            Connection = new HubConnection(constants.ConnectionHubUrl, querystringData);
            Proxy = Connection.CreateHubProxy("ConnectionHub");
            //Proxy.On("ReturnPing", (int restDay, int restHour, int restMinute, int restSecond) =>
            //{
            //    if (restDay == 0 && restHour == 0 && restMinute == 0 && restSecond == 0)
            //    {
            //        MessageBox.Show("Your account has expired! Please contact to administrator for infomation!");
            //    }
            //    else
            //    {
            //        RestDay = restDay;
            //        RestHour = restHour;
            //        RestMinute = restMinute;
            //        RestSecond = restSecond;
            //    }
            //});
            Connection.Start().Wait();
        }
    }
}
