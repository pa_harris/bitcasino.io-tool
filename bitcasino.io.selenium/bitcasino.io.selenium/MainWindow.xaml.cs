﻿using bitcasino.io.selenium.Helpers;
using bitcasino.io.selenium.Utilities;
using bitcasino.io.selenium.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace bitcasino.io.selenium
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #region Properties
        private string _UserName;
        public string UserName { get => _UserName; set { _UserName = value; OnPropertyChanged(); } }
        private string _Password;
        public string Password { get => _Password; set { _Password = value; OnPropertyChanged(); } }
        private string _GameSelected;
        public string GameSelected { get => _GameSelected; set { _GameSelected = value; OnPropertyChanged(); } }
        private ObservableCollection<string> _ListGame = new ObservableCollection<string>();
        public ObservableCollection<string> ListGame { get => _ListGame; set { _ListGame = value; OnPropertyChanged(); } }

        string filePath = "username.txt";
        #endregion

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            FirstLoad();
        }

        private void FirstLoad()
        {
            constants.AccessToken = null;
            constants.Account = null;
            constants.CurrentAccs = null;
            ListGame.Add("bitcasino.io");
            ListGame.Add("bigplayerclub.com");
            ListGame.Add("southqueenpro.com");
            ListGame.Add("studio.evolutiongaming.com");
            GameSelected = ListGame.FirstOrDefault();
            try
            {
                UserName = File.ReadAllText(filePath);
            }
            catch (Exception ee)
            {
            }
        }

        private async void Login(object sender, RoutedEventArgs e)
        {
            if (!constants.IsCheckServer)
            {
                LoadGame();
                return;
            }
            if (ValidateData())
            {
                bool isLoggedIn = await UserUtilities.Login(new Models.AccountLoginModel
                {
                    UserName = this.UserName,
                    Password = this.Password
                }, (o) =>
                {
                    MessageBox.Show(o);
                });

                if (isLoggedIn)
                {
                    //var accs = await AccsUtilities.GetAccs((o) =>
                    //{
                    //    MessageBox.Show(o);
                    //});
                    var isGotUserInfo = await UserUtilities.GetUserInfo((o) =>
                    {
                        MessageBox.Show(o);
                    });
                    if (/*accs != null && */isGotUserInfo)
                    {
                        using (StreamWriter sw = System.IO.File.CreateText(filePath))
                        {
                            sw.Write(UserName);
                        }
                        BotWindow botWindow = new BotWindow(GameSelected);
                        this.Close();
                        botWindow.Show(); 
                    }
                }
            }
        }

        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(UserName))
            {
                MessageBox.Show("UserName không được trống!");
                return false;
            }
            if (string.IsNullOrEmpty(Password))
            {
                MessageBox.Show("Password không được trống!");
                return false;
            }
            if (string.IsNullOrEmpty(GameSelected))
            {
                MessageBox.Show("Game không được trống!");
                return false;
            }
            if (DateTime.Now > (new DateTime(2021, 01, 27)).AddDays(14))
            {
                this.Close();
                return false;
            }
            return true;
        }
        private void LoadGame()
        {
            switch (GameSelected)
            {
                case "bitcasino.io":
                    BotWindow botWindow = new BotWindow();
                    this.Close();
                    botWindow.Show();
                    break;
                case "bigplayerclub.com":
                    BigPlayerClub bigPlayerClub = new BigPlayerClub();
                    this.Close();
                    bigPlayerClub.Show();
                    break;
                case "southqueenpro.com":
                    SouthQueenPro southQueenPro = new SouthQueenPro();
                    this.Close();
                    southQueenPro.Show();
                    break;
                default:
                    MessageBox.Show("Something went wrong! Game not found!");
                    break;
            }
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }
    }
}
