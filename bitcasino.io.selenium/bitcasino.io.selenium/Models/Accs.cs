﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bitcasino.io.selenium.Models
{
    public class Accs
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string PasswordRaw { get; set; }
    }
}
