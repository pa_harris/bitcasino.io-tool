﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bitcasino.io.selenium.Models
{
    public class UserAccountInfo
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string PasswordUser { get; set; }
        public int? Duration { get; set; }
        public DateTime ActiveDate { get; set; }
    }
}
