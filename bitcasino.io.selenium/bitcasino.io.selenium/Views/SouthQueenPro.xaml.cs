﻿using bitcasino.io.selenium.Helpers;
using bitcasino.io.selenium.Models;
using bitcasino.io.selenium.Utilities;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using static bitcasino.io.selenium.Utilities.BettingUtilities;

namespace bitcasino.io.selenium.Views
{
    /// <summary>
    /// Interaction logic for SouthQueenPro.xaml
    /// </summary>
    public partial class SouthQueenPro : Window, INotifyPropertyChanged
    {
        #region Helpers
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        private void WriteLog(string log)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                txbLog.Text = log + Environment.NewLine + txbLog.Text;
            }));

            #region Write to file
            string path = "Logs\\SouthQueenPro";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = "Logs\\SouthQueenPro\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!System.IO.File.Exists(filepath))
            {
                // Create a file to write to.
                using (StreamWriter sw = System.IO.File.CreateText(filepath))
                {
                    sw.WriteLine(log);
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(filepath))
                {
                    sw.WriteLine(log);
                }
            }
            #endregion
        }
        #endregion

        #region Properties

        #region Helpers
        string restart = "Unstable connection! Please check your connection and restart your BOT!";
        private ChromeDriver driver;
        string ProfileFolderPath = "Profile";
        Thread t;
        DispatcherTimer timer;
        #endregion

        #region SignalR

        #endregion

        #region TargetUser
        private bool _Admin;
        public bool Admin
        {
            get => _Admin;
            set
            {
                _Admin = value;
                if (Admin)
                {
                    IsAdmin = Visibility.Visible;
                    IsNotAdmin = Visibility.Hidden;
                }
                else
                {
                    IsAdmin = Visibility.Hidden;
                    IsNotAdmin = Visibility.Visible;
                }

            }
        }
        private Visibility _IsAdmin = Visibility.Visible;
        public Visibility IsAdmin { get => _IsAdmin; set { _IsAdmin = value; OnPropertyChanged(); } }
        private Visibility _IsNotAdmin = Visibility.Hidden;
        public Visibility IsNotAdmin { get => _IsNotAdmin; set { _IsNotAdmin = value; OnPropertyChanged(); } }
        #endregion

        #region Timer
        private int _RestDay;
        public int RestDay { get => _RestDay; set { _RestDay = value; OnPropertyChanged(); } }
        private int _RestMinute;
        public int RestMinute { get => _RestMinute; set { _RestMinute = value; OnPropertyChanged(); } }
        private int _RestHour;
        public int RestHour { get => _RestHour; set { _RestHour = value; OnPropertyChanged(); } }
        private int _RestSecond;
        public int RestSecond { get => _RestSecond; set { _RestSecond = value; OnPropertyChanged(); } }
        #endregion

        #region CoinElements
        // 1 2 5 25 100 500 2500
        private List<IWebElement> _ListCoin = new List<IWebElement>(7);
        public List<IWebElement> ListCoin { get => _ListCoin; set { _ListCoin = value; OnPropertyChanged(); } }
        private List<int> _ListCoinCount = new List<int>(9) { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        public List<int> ListCoinCount { get => _ListCoinCount; set { _ListCoinCount = value; OnPropertyChanged(); } }
        #endregion

        #region WebElements
        IWebElement banker = null;
        IWebElement player = null;
        IWebElement status = null;
        IWebElement balance = null;
        IWebElement tienVon = null;
        IWebElement doubleElement = null;
        IWebElement totalBet = null;
        IWebElement labelTotalBet = null;
        IWebElement gameName = null;
        #endregion

        #region Betting infomation
        private ObservableCollection<BetItem> _ListSelection = new ObservableCollection<BetItem>(BettingUtilities.ListSelection);
        public ObservableCollection<BetItem> ListSelection { get => _ListSelection; set { _ListSelection = value; OnPropertyChanged(); } }

        private ObservableCollection<BetItem> _ListPlay = new ObservableCollection<BetItem>();
        public ObservableCollection<BetItem> ListPlay { get => _ListPlay; set { _ListPlay = value; OnPropertyChanged(); } }
        #endregion

        #region Betting money
        private double _TienVon = 0;
        public double TienVon { get => _TienVon; set { _TienVon = value; OnPropertyChanged(); } }
        private double _TienBalance = 0;
        public double TienBalance { get => _TienBalance; set { _TienBalance = value; OnPropertyChanged(); } }
        private decimal _TienCuoc;
        public decimal TienCuoc { get => _TienCuoc; set { _TienCuoc = value; CurrentBet = value; OnPropertyChanged(); } }
        private decimal _CurrentBet;
        public decimal CurrentBet { get => _CurrentBet; set { _CurrentBet = value; OnPropertyChanged(); } }
        private decimal _CurrentSuccessBet;
        public decimal CurrentSuccessBet { get => _CurrentSuccessBet; set { _CurrentSuccessBet = value; OnPropertyChanged(); WriteLog($"CurrentSuccessBet = {value}"); } }
        private decimal _TotalBetMoney = 0;
        public decimal TotalBetMoney { get => _TotalBetMoney; set { _TotalBetMoney = value; OnPropertyChanged(); WriteLog($"TotalBetMoney = {value}"); } }
        #endregion

        #region CheckBox infomation
        private bool _IsRepeat;
        public bool IsRepeat { get => _IsRepeat; set { _IsRepeat = value; OnPropertyChanged(); } }
        private bool _IsDouble;
        public bool IsDouble { get => _IsDouble; set { _IsDouble = value; OnPropertyChanged(); } }
        private bool _IsStopWhenWin;
        public bool IsStopWhenWin { get => _IsStopWhenWin; set { _IsStopWhenWin = value; OnPropertyChanged(); } }
        #endregion

        #region Account infomation
        private Accs _Accs;
        public Accs Accs { get => _Accs; set { _Accs = value; OnPropertyChanged(); } }
        private UserAccountInfo _Account;
        public UserAccountInfo Account { get => _Account; set { _Account = value; OnPropertyChanged(); } }
        #endregion

        #region Result infomation
        private int _Win = 0;
        public int Win { get => _Win; set { _Win = value; OnPropertyChanged(); } }
        private int _Lose = 0;
        public int Lose { get => _Lose; set { _Lose = value; OnPropertyChanged(); } }
        private int _Tie = 0;
        public int Tie { get => _Tie; set { _Tie = value; OnPropertyChanged(); } }

        private int _LoseContinuity = 9;
        public int LoseContinuity { get => _LoseContinuity; set { _LoseContinuity = value; OnPropertyChanged(); } }
        private int _CountLoseContinuity = 0;
        public int CountLoseContinuity { get => _CountLoseContinuity; set { _CountLoseContinuity = value; OnPropertyChanged(); } }
        #endregion

        #region Ways collection
        Dictionary<int, int[]> ListWay1 = new Dictionary<int, int[]>();
        Dictionary<int, int[]> ListWay2 = new Dictionary<int, int[]>();
        Dictionary<int, int[]> ListWay3 = new Dictionary<int, int[]>();
        Dictionary<int, int[]> ListWay4 = new Dictionary<int, int[]>();
        Dictionary<int, int[]> ListWay5 = new Dictionary<int, int[]>();
        Dictionary<int, int[]> ListWay6 = new Dictionary<int, int[]>();
        Dictionary<int, int[]> ListWay7 = new Dictionary<int, int[]>();
        #endregion

        #endregion

        public SouthQueenPro()
        {
            InitializeComponent();
            DataContext = this;

            Admin = BettingUtilities.TargetUser == ETargetUser.Admin;

            FirstLoad();
        }
        private void FirstLoad()
        {
            if (constants.IsCheckServer)
            {
                Account = constants.Account;
                Accs = constants.CurrentAccs;
                //ConnectSignalR();
                timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromSeconds(1);
                timer.Tick += timer_Tick;
                timer.Start();
            }

            #region Init bot selection
            WriteLog("Creating calculation");
            ListPlay.Add(ListSelection.FirstOrDefault());
            ListPlay.Add(ListSelection.FirstOrDefault());
            ListPlay.Add(ListSelection.FirstOrDefault());
            ListPlay.Add(ListSelection.FirstOrDefault());
            ListPlay.Add(ListSelection.LastOrDefault());
            ListPlay.Add(ListSelection.LastOrDefault());
            ListPlay.Add(ListSelection.FirstOrDefault());
            ListPlay.Add(ListSelection.LastOrDefault());
            IsRepeat = true; IsDouble = true;
            WriteLog("Calculation created");
            #endregion

            LoadWays();
            OpenChromeDriver();
        }
        private void timer_Tick(object sender, EventArgs e)
        {
            //this.Proxy.Invoke("SendPing");
        }
        private void OpenChromeDriver()
        {
            if (driver != null)
            {
                try
                {
                    driver.Close();
                    driver.Quit();
                }
                catch (Exception ex)
                {
                }
            }
            ChromeOptions options = new ChromeOptions();

            if (!Directory.Exists(ProfileFolderPath))
            {
                Directory.CreateDirectory(ProfileFolderPath);
            }
            if (Directory.Exists(ProfileFolderPath))
            {
                options.AddArguments("user-data-dir=" + ProfileFolderPath);
            }
            //options.AddArgument("--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36");
            options.AddExcludedArgument("enable-automation");
            options.AddAdditionalCapability("useAutomationExtension", false);
            options.AddArguments("--disable-blink-features=AutomationControlled");
#if DEBUG
            driver = new ChromeDriver(@"D:\NA\Downloads\chromedriver_win32", options);
#else
            driver = new ChromeDriver(options);
#endif
            driver.Url = "https://southqueenpro.com";
            driver.Navigate();
        }
        private void LoadWays()
        {
            try
            {
                WriteLog("Loading gameplay");
                string path = "ways.txt";

                string[] s = File.ReadAllLines(path);
                // ứng với index là thứ tự cách. 
                // 1 . 1 5 25 100 250 500
                // 2 . 1 5 25 100 250 2500
                // 3 . 1 5 25 100 500 2500
                // 4 . 1 5 25 100 1000 2500
                // 5 . 2 5 25 100 250 2500
                // 6 . 2 5 25 100 500 2500
                // 7 . 2 5 25 100 1000 2500
                int wayIndex = 1;

                foreach (var item in s)
                {
                    if (string.IsNullOrEmpty(item))
                    {
                        wayIndex++;
                        continue;
                    }
                    else
                    {
                        var data = item.Split('.')[1].Trim().Split(' ');
                        int[] res = Array.ConvertAll(data, c => (int)Char.GetNumericValue(c.FirstOrDefault()));
                        switch (wayIndex)
                        {
                            case 1:
                                ListWay1.Add(Convert.ToInt32(item.Split('.')[0].Trim()), res);
                                break;
                            case 2:
                                ListWay2.Add(Convert.ToInt32(item.Split('.')[0].Trim()), res);
                                break;
                            case 3:
                                ListWay3.Add(Convert.ToInt32(item.Split('.')[0].Trim()), res);
                                break;
                            case 4:
                                ListWay4.Add(Convert.ToInt32(item.Split('.')[0].Trim()), res);
                                break;
                            case 5:
                                ListWay5.Add(Convert.ToInt32(item.Split('.')[0].Trim()), res);
                                break;
                            case 6:
                                ListWay6.Add(Convert.ToInt32(item.Split('.')[0].Trim()), res);
                                break;
                            case 7:
                                ListWay7.Add(Convert.ToInt32(item.Split('.')[0].Trim()), res);
                                break;
                            default:
                                break;
                        }
                    }
                }

                WriteLog("Gameplay loaded");
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private bool ValidateInfo()
        {
            if (TienCuoc <= 0)
            {
                string error = "Betting money must be more than 0";
                MessageBox.Show(error);
                WriteLog(error);
                return false;
            }
            if (ListPlay.Count() != 8)
            {
                string error = $"ListPlay.Count != 8. Please logout and restart bot!";
                MessageBox.Show(error);
                WriteLog(error);
                return false;
            }
            foreach (var item in ListPlay)
            {
                if (string.IsNullOrEmpty(item?.Value))
                {
                    string error = $"ListPlay.Item is empty. Please logout and restart bot!";
                    MessageBox.Show(error);
                    WriteLog(error);
                    return false;
                }
            }

            return true;
        }
        private void ReloadButtonStart(string text)
        {
            this.Dispatcher.Invoke(() =>
            {
                btnStart.Content = text;
            });
        }
        private bool FirstLoadClick()
        {
            CurrentBet = TienCuoc;

            string message = "";
            balance = SeleniumHelper.FindElement(driver, By.ClassName("amount--3AxPZ"), out message);
            if (message.Contains("Found"))
            {
                string tienVonText = balance.Text.Replace("$", "").Trim();
                TienVon = Convert.ToDouble(tienVonText);
            }


            #region Get tiền vốn
            try
            {
                tienVon = driver.FindElementByXPath("//*[@id=\"profileButton\"]/div/p[2]");
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show("Cannot get the starting money! Please restart bot!");
            }
            if (tienVon == null)
            {
                WriteLog("Cannot get the starting money element!");
                return false;
            }
            else
            {
                string tienVonText = tienVon.Text.Split(' ').FirstOrDefault().Trim();
                TienVon = Convert.ToDouble(tienVonText);
            }
            #endregion

            #region Get gamename
            try
            {
                gameName = driver.FindElementByClassName("tableName--3PUPn");
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show("Cannot get the game name! Please restart bot!");
            }
            if (gameName == null)
            {
                WriteLog("Cannot get the game name element!");
                return false;
            }
            #endregion

            #region Finding banker
            WriteLog("Finding banker");
            try
            {
                banker = driver.FindElementByClassName("banker--Q8hMb");
                if (banker != null)
                {
                    WriteLog("Banker found");
                }
                else
                {
                    WriteLog($"Banker not found! {restart}");
                    return false;
                }
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show("Please restart bot!");
                return false;
            }
            #endregion

            #region Finding player
            WriteLog("Finding player");
            try
            {
                player = driver.FindElementByClassName("player--1nDyW");
                if (player != null)
                {
                    WriteLog("Player found");
                }
                else
                {
                    WriteLog($"Player not found! {restart}");
                    return false;
                }
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show("Please restart bot!");
                return false;
            }
            #endregion

            #region Finding status
            WriteLog("Finding status");
            try
            {
                status = driver.FindElementByClassName("text--1jzYQ");
                if (status != null)
                {
                    WriteLog("Status found");
                }
                else
                {
                    WriteLog($"Status not found! {restart}");
                    return false;
                }
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            #endregion

            #region Finding balance
            WriteLog("Finding balance");
            try
            {
                balance = driver.FindElementByClassName("amount--3AxPZ");
                if (balance != null)
                {
                    WriteLog("Balance found");
                    ReloadBalance();
                }
                else
                {
                    WriteLog($"Balance not found! {restart}");
                    return false;
                }
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            #endregion

            //amount--3BgvQ
            //title--GvCvW
            #region Finding total bet
            WriteLog("Finding total bet");
            try
            {
                totalBet = driver.FindElementByClassName("amount--3BgvQ");
                if (totalBet != null)
                {
                    WriteLog("Total bet found");
                    ReloadBalance();
                }
                else
                {
                    WriteLog($"totalBet not found! {restart}");
                    return false;
                }
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            #endregion

            #region Tìm label total bet
            WriteLog("Finding total bet");
            try
            {
                labelTotalBet = driver.FindElementByClassName("title--GvCvW");
                if (labelTotalBet != null)
                {
                    WriteLog("labelTotalBet found!");
                    ReloadBalance();
                }
                else
                {
                    WriteLog($"labelTotalBet not found! {restart}");
                    return false;
                }
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            #endregion

            #region Reset collection
            ListCoin.Clear();
            ListCoinCount[0] = 0;
            ListCoinCount[1] = 0;
            ListCoinCount[2] = 0;
            ListCoinCount[3] = 0;
            ListCoinCount[4] = 0;
            ListCoinCount[5] = 0;
            ListCoinCount[6] = 0;
            #endregion

            #region Finding coins
            WriteLog("Finding coins");
            try
            {
                ReadOnlyCollection<IWebElement> lstChipClass = null; // đây là element có text là giá tiền
                if (gameName.Text.Equals("Baccarat A") || gameName.Text.Equals("Baccarat B") || gameName.Text.Equals("Baccarat C"))
                {
                    lstChipClass = driver.FindElementsByClassName("chip--1itRi");
                }
                else
                {
                    lstChipClass = driver.FindElementsByClassName("chip--1itRi");
                }

                ListCoin.Add(GetCoinElement(lstChipClass, 1));
                ListCoin.Add(GetCoinElement(lstChipClass, 2));
                ListCoin.Add(GetCoinElement(lstChipClass, 5));
                ListCoin.Add(GetCoinElement(lstChipClass, 25));
                ListCoin.Add(GetCoinElement(lstChipClass, 100));
                ListCoin.Add(GetCoinElement(lstChipClass, 250));
                ListCoin.Add(GetCoinElement(lstChipClass, 500));
                ListCoin.Add(GetCoinElement(lstChipClass, 1000));
                ListCoin.Add(GetCoinElement(lstChipClass, 2500));

                int count = ListCoin.Count(x => x != null);

                if (count != 6)
                {
                    WriteLog($"ListCoin.Count != 6. ListCoin.Count = {count}");
                    return false;
                }

                WriteLog("Got the coins");
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            #endregion

            driver.SwitchTo().DefaultContent();

            return true;
        }
        private void Start(object sender, RoutedEventArgs e)
        {
            WriteLog("-------------------------------------------------");
            if (btnStart.Content.ToString().Equals("Start"))
            {
                btnStart.Content = "Stop";
                t = new Thread(() =>
                {
                    bool isDoneFirstLoadClick = FirstLoadClick();
                });
            }
            else
            {
                try
                {
                    t.Abort();
                    ReloadButtonStart("Start");
                }
                catch (Exception ee)
                {
                    WriteLog(ee.Message);
                }
            }
        }
        private void ReloadBalance()
        {
            if (balance == null)
            {
                WriteLog("Balance element not found!");
                return;
            }
            else
            {
                string balanceText = Regex.Replace(balance.Text, "[^0-9.]", "");
                WriteLog($"Balance text {balanceText}");
                if (!string.IsNullOrEmpty(balanceText))
                {
                    TienBalance = Convert.ToDouble(balanceText);
                    OnPropertyChanged(nameof(TienBalance));
                    WriteLog(TienBalance.ToString());
                }
            }
        }
        private IWebElement GetCoinElement(ReadOnlyCollection<IWebElement> listCoin, int money)
        {
            IWebElement res = null;

            try
            {
                res = listCoin.FirstOrDefault((p) => p.GetAttribute("data-role").Equals("chip") && p.GetAttribute("data-value").Equals($"{money}"));
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }
            return res;
        }
    }
}
