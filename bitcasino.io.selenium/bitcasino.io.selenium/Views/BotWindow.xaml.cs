﻿using bitcasino.io.selenium.Helpers;
using bitcasino.io.selenium.Models;
using bitcasino.io.selenium.Utilities;
using Microsoft.AspNet.SignalR.Client;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using static bitcasino.io.selenium.Utilities.BettingUtilities;

namespace bitcasino.io.selenium.Views
{
    /// <summary>
    /// Interaction logic for BotWindow.xaml
    /// </summary>
    public partial class BotWindow : Window, INotifyPropertyChanged
    {
        #region Helpers
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        private void WriteLog(string log)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                txbLog.Text = log + Environment.NewLine + txbLog.Text;
            }));

            #region Write to file

            string path = $"Logs\\{WebName}";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = $"Logs\\{WebName}\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!System.IO.File.Exists(filepath))
            {
                // Create a file to write to.
                using (StreamWriter sw = System.IO.File.CreateText(filepath))
                {
                    sw.WriteLine(log);
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(filepath))
                {
                    sw.WriteLine(log);
                }
            }

            #endregion
        }
        #endregion

        #region Properties
        string restart = "Unstable connection! Please check your connection and restart your BOT!";
        private ChromeDriver driver;
        string ProfileFolderPath = "Profile";
        public IHubProxy Proxy { get; set; }
        public HubConnection Connection { get; set; }

        private UserAccountInfo _Account;
        public UserAccountInfo Account { get => _Account; set { _Account = value; OnPropertyChanged(); } }


        private bool _Admin;
        public bool Admin
        {
            get => _Admin;
            set
            {
                _Admin = value;
                if (Admin)
                {
                    IsAdmin = Visibility.Visible;
                    IsNotAdmin = Visibility.Hidden;
                }
                else
                {
                    IsAdmin = Visibility.Hidden;
                    IsNotAdmin = Visibility.Visible;
                }

            }
        }

        private Visibility _IsAdmin = Visibility.Visible;
        public Visibility IsAdmin { get => _IsAdmin; set { _IsAdmin = value; OnPropertyChanged(); } }

        private Visibility _IsNotAdmin = Visibility.Hidden;
        public Visibility IsNotAdmin { get => _IsNotAdmin; set { _IsNotAdmin = value; OnPropertyChanged(); } }

        private string _WebName;
        public string WebName { get => _WebName; set { _WebName = value; OnPropertyChanged(); } }

        private Accs _Accs;
        public Accs Accs { get => _Accs; set { _Accs = value; OnPropertyChanged(); } }
        private int _RestDay;
        public int RestDay { get => _RestDay; set { _RestDay = value; OnPropertyChanged(); } }
        private int _RestMinute;
        public int RestMinute { get => _RestMinute; set { _RestMinute = value; OnPropertyChanged(); } }
        private int _RestHour;
        public int RestHour { get => _RestHour; set { _RestHour = value; OnPropertyChanged(); } }
        private int _RestSecond;
        public int RestSecond { get => _RestSecond; set { _RestSecond = value; OnPropertyChanged(); } }

        private int _StopWhenWinPercent = 3;
        public int StopWhenWinPercent { get => _StopWhenWinPercent; set { _StopWhenWinPercent = value; OnPropertyChanged(); } }

        private ObservableCollection<BetItem> _ListSelection = new ObservableCollection<BetItem>(BettingUtilities.ListSelection);
        public ObservableCollection<BetItem> ListSelection { get => _ListSelection; set { _ListSelection = value; OnPropertyChanged(); } }

        private ObservableCollection<BetItem> _ListPlay = new ObservableCollection<BetItem>();
        public ObservableCollection<BetItem> ListPlay { get => _ListPlay; set { _ListPlay = value; OnPropertyChanged(); } }
        private double _TienVon = 0;
        public double TienVon { get => _TienVon; set { _TienVon = value; OnPropertyChanged(); } }
        private double _TienBalance = 0;
        public double TienBalance { get => _TienBalance; set { _TienBalance = value; OnPropertyChanged(); } }
        private int _LoseContinuity = 9;
        public int LoseContinuity { get => _LoseContinuity; set { _LoseContinuity = value; OnPropertyChanged(); } }
        private decimal _TienCuoc;
        public decimal TienCuoc { get => _TienCuoc; set { _TienCuoc = value; CurrentBet = value; OnPropertyChanged(); } }
        private decimal _CurrentBet;
        public decimal CurrentBet { get => _CurrentBet; set { _CurrentBet = value; OnPropertyChanged(); } }
        private decimal _CurrentSuccessBet;
        public decimal CurrentSuccessBet { get => _CurrentSuccessBet; set { _CurrentSuccessBet = value; OnPropertyChanged(); WriteLog($"CurrentSuccessBet = {value}"); } }
        private bool _IsRepeat;
        public bool IsRepeat { get => _IsRepeat; set { _IsRepeat = value; OnPropertyChanged(); } }
        private bool _IsDouble;
        public bool IsDouble { get => _IsDouble; set { _IsDouble = value; OnPropertyChanged(); } }
        private bool _IsStopWhenWin;
        public bool IsStopWhenWin { get => _IsStopWhenWin; set { _IsStopWhenWin = value; OnPropertyChanged(); } }
        private int _Win = 0;
        public int Win { get => _Win; set { _Win = value; OnPropertyChanged(); } }
        private int _Lose = 0;
        public int Lose { get => _Lose; set { _Lose = value; OnPropertyChanged(); } }
        private int _Tie = 0;
        public double Profit { get => _Profit; set { _Profit = value; OnPropertyChanged(); IsProfitDown = Profit < 0; } }
        private double _Profit = 0;
        public bool IsProfitDown { get => _IsProfitDown; set { _IsProfitDown = value; OnPropertyChanged(); } }
        private bool _IsProfitDown = false;

        public int Tie { get => _Tie; set { _Tie = value; OnPropertyChanged(); } }
        private int _CountLoseContinuity = 0;
        public int CountLoseContinuity { get => _CountLoseContinuity; set { _CountLoseContinuity = value; OnPropertyChanged(); } }
        private decimal _TotalBetMoney = 0;
        public decimal TotalBetMoney { get => _TotalBetMoney; set { _TotalBetMoney = value; OnPropertyChanged(); WriteLog($"TotalBetMoney = {value}"); } }

        // 1 2 5 25 100 500 2500
        private List<IWebElement> _ListCoin = new List<IWebElement>(7);
        public List<IWebElement> ListCoin { get => _ListCoin; set { _ListCoin = value; OnPropertyChanged(); } }
        private List<int> _ListCoinCount = new List<int>(9) { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        public List<int> ListCoinCount { get => _ListCoinCount; set { _ListCoinCount = value; OnPropertyChanged(); } }

        Dictionary<int, int[]> ListWay1 = new Dictionary<int, int[]>();
        Dictionary<int, int[]> ListWay2 = new Dictionary<int, int[]>();
        Dictionary<int, int[]> ListWay3 = new Dictionary<int, int[]>();
        Dictionary<int, int[]> ListWay4 = new Dictionary<int, int[]>();
        Dictionary<int, int[]> ListWay5 = new Dictionary<int, int[]>();
        Dictionary<int, int[]> ListWay6 = new Dictionary<int, int[]>();
        Dictionary<int, int[]> ListWay7 = new Dictionary<int, int[]>();
        Dictionary<int, int[]> ListWay8 = new Dictionary<int, int[]>();

        Thread t;
        DispatcherTimer timer;

        #endregion

        #region Một số element get được
        // Tiền balance: class=amount--3AxPZ
        // Player: class=player--1nDyW
        // Banker: class=banker--Q8hMb
        // Ô 1USD: class=chip--1itRi shadow--2_7Gl
        // Status bar: class=text--1jzYQ uppercase--tL_HU font-small--g0aus
        IWebElement banker = null;
        IWebElement player = null;
        IWebElement status = null;
        IWebElement balance = null;
        IWebElement tienVon = null;
        IWebElement doubleElement = null;
        IWebElement totalBet = null;
        IWebElement labelTotalBet = null;
        IWebElement gameName = null;
        IWebElement timerElement = null;
        #endregion


        #region js
        string jsClickBanker = "document.getElementsByClassName('banker--Q8hMb')[0].click()";
        string jsClickPlayer = "document.getElementsByClassName('player--1nDyW')[0].click()";

        #endregion

        public BotWindow(string webName = "bitcasino.io")
        {
            InitializeComponent();
            DataContext = this;

            Admin = BettingUtilities.TargetUser == ETargetUser.Admin;
            WebName = webName;

            FirstLoad();
        }
        private string LoadUserAgent()
        {
            try
            {
                WriteLog("Loading user agent");
                string path = "useragent.txt";

                string s = File.ReadAllLines(path).FirstOrDefault();

                WriteLog($"User agent: {s}");
                return s;
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                //MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return "";
        }
        private void LoadWays()
        {
            try
            {
                WriteLog("Loading gameplay");
                string path = "ways.txt";

                string[] s = File.ReadAllLines(path);
                // ứng với index là thứ tự cách. 
                // 1 . 1 5 25 100 250 500
                // 2 . 1 5 25 100 250 2500
                // 3 . 1 5 25 100 500 2500
                // 4 . 1 5 25 100 1000 2500
                // 5 . 2 5 25 100 250 2500
                // 6 . 2 5 25 100 500 2500
                // 7 . 2 5 25 100 1000 2500
                // 8 . 1 5 25 100 500 1000
                int wayIndex = 1;

                foreach (var item in s)
                {
                    if (string.IsNullOrEmpty(item))
                    {
                        wayIndex++;
                        continue;
                    }
                    else
                    {
                        var data = item.Split('.')[1].Trim().Split(' ');
                        int[] res = Array.ConvertAll(data, c => (int)Char.GetNumericValue(c.FirstOrDefault()));
                        switch (wayIndex)
                        {
                            case 1:
                                ListWay1.Add(Convert.ToInt32(item.Split('.')[0].Trim()), res);
                                break;
                            case 2:
                                ListWay2.Add(Convert.ToInt32(item.Split('.')[0].Trim()), res);
                                break;
                            case 3:
                                ListWay3.Add(Convert.ToInt32(item.Split('.')[0].Trim()), res);
                                break;
                            case 4:
                                ListWay4.Add(Convert.ToInt32(item.Split('.')[0].Trim()), res);
                                break;
                            case 5:
                                ListWay5.Add(Convert.ToInt32(item.Split('.')[0].Trim()), res);
                                break;
                            case 6:
                                ListWay6.Add(Convert.ToInt32(item.Split('.')[0].Trim()), res);
                                break;
                            case 7:
                                ListWay7.Add(Convert.ToInt32(item.Split('.')[0].Trim()), res);
                                break;
                            case 8:
                                ListWay8.Add(Convert.ToInt32(item.Split('.')[0].Trim()), res);
                                break;
                            default:
                                break;
                        }
                    }
                }

                WriteLog("Gameplay loaded");
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void FirstLoad()
        {
            if (constants.IsCheckServer)
            {
                Account = constants.Account;
                Accs = constants.CurrentAccs;
            }

            #region Init bot selection
            WriteLog("Creating calculation");
            ListPlay.Add(ListSelection.FirstOrDefault());
            ListPlay.Add(ListSelection.FirstOrDefault());
            ListPlay.Add(ListSelection.FirstOrDefault());
            ListPlay.Add(ListSelection.FirstOrDefault());
            ListPlay.Add(ListSelection.LastOrDefault());
            ListPlay.Add(ListSelection.LastOrDefault());
            ListPlay.Add(ListSelection.FirstOrDefault());
            ListPlay.Add(ListSelection.LastOrDefault());

            IsRepeat = true; IsDouble = true;
            WriteLog("Calculation created");
            #endregion

            LoadWays();

            if (constants.IsCheckServer)
            {
                ConnectSignalR();
                timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromSeconds(1);
                timer.Tick += timer_Tick;
                timer.Start();
            }

            OpenChromeDriver();
        }
        private void timer_Tick(object sender, EventArgs e)
        {
            this.Proxy.Invoke("SendPing");
        }
        private void ConnectSignalR()
        {
            WriteLog("Connecting to server");
            var querystringData = new Dictionary<string, string>();
            querystringData.Add("bearerToken", constants.AccessToken);
            this.Connection = new HubConnection(constants.ConnectionHubUrl, querystringData);
            this.Proxy = Connection.CreateHubProxy("ConnectionHub");
            this.Proxy.On("ReturnPing", (int restDay, int restHour, int restMinute, int restSecond) =>
            {
                if (restDay == 0 && restHour == 0 && restMinute == 0 && restSecond == 0)
                {
                    MessageBox.Show("Your account has expired! Please contact to administrator for infomation!");
                    ExitApp();
                }
                else
                {
                    RestDay = restDay;
                    RestHour = restHour;
                    RestMinute = restMinute;
                    RestSecond = restSecond;
                }
            });
            this.Connection.Start().Wait();
            WriteLog("Server connected");
        }

        private void SwitchFrame()
        {
            try
            {
                driver.SwitchTo().DefaultContent();
            }
            catch (Exception ee)
            {
                WriteLog($"Error switching default frame!!!");
                WriteLog(ee.Message);
                return;
            }
            string frameId = "";
            switch (WebName)
            {
                case "bitcasino.io":
                    frameId = "gameIframe";
                    break;
                case "bigplayerclub.com":
                    frameId = "casinoGame";
                    break;
                case "southqueenpro.com":
                    frameId = "casinoGame";
                    break;
                case "studio.evolutiongaming.com":
                    frameId = "";
                    break;
                default:
                    MessageBox.Show("Something went wrong! Game not found!");
                    break;
            }

            #region Switch Frame
            if (!string.IsNullOrEmpty(frameId))
            {
                WriteLog("Switch Frame");
                IWebElement frame = null;
                try
                {
                    frame= driver.FindElement(By.Id(frameId));
                }
                catch { }
                if (frame == null)
                {
                    WriteLog($"Frame not found! {restart}");
                    return;
                }

                try
                {
                    driver.SwitchTo().Frame(frame);
                    IWebElement frame2 = driver.FindElement(By.XPath("/html/body/div[7]/div[1]/iframe"));
                    driver.SwitchTo().Frame(frame2);
                }
                catch (Exception ee)
                {
                    WriteLog($"Error switching frame!!!");
                    WriteLog(ee.Message);
                    return;
                }
            }
            #endregion
        }
        private IWebElement GetCoinElement(ReadOnlyCollection<IWebElement> listCoin, int money)
        {
            IWebElement res = null;

            try
            {
                res = listCoin.FirstOrDefault((p) => p.GetAttribute("data-role").Equals("chip") && p.GetAttribute("data-value").Equals($"{money}"));
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                //MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }
            return res;
        }



        private bool FirstLoadClick()
        {
            CurrentBet = TienCuoc;
            try
            {
                driver.SwitchTo().DefaultContent();
            }
            catch (Exception ee)
            {
                WriteLog($"Error switching default frame!!!");
                WriteLog(ee.Message);
                return false;
            }

            #region Get tiền vốn
            if (WebName == "bitcasino.io")
            {
                try
                {
                    tienVon = driver.FindElementByXPath("//*[@id=\"profileButton\"]/div/p[2]");
                }
                catch (Exception ee)
                {
                    WriteLog(ee.Message);
                    MessageBox.Show("Cannot get the starting money! Please restart bot!");
                }
                if (tienVon == null)
                {
                    WriteLog("Cannot get the starting money element!");
                    //return false;
                }
                else
                {
                    string tienVonText = tienVon.Text.Split(' ').FirstOrDefault().Trim();
                    TienVon = Convert.ToDouble(tienVonText, CultureInfo.GetCultureInfo("En")) ;
                }
            }
            else if (WebName == "southqueenpro.com" || WebName == "bigplayerclub.com")
            {
                SwitchFrame();
                #region Finding balance
                WriteLog("Finding balance");
                try
                {
                    balance = null;
                    try
                    {
                        balance = driver.FindElementByClassName("amount--3AxPZ");
                    }
                    catch { }
                    if (balance != null)
                    {
                        WriteLog("Balance found");
                        ReloadBalance();
                    }
                    else
                    {
                        WriteLog($"Balance not found! {restart}");
                        return false;
                    }
                }
                catch (Exception ee)
                {
                    WriteLog(ee.Message);
                    MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }

                string balanceText = Regex.Replace(balance.Text, "[^0-9.]", "");
                WriteLog($"Balance text {balanceText}");
                if (!string.IsNullOrEmpty(balanceText))
                {
                    TienVon = Convert.ToDouble(balanceText, CultureInfo.GetCultureInfo("En"));
                    OnPropertyChanged(nameof(TienVon));
                }
                #endregion
                driver.SwitchTo().DefaultContent();
            }

            #endregion

            SwitchFrame();

            #region Get gamename
            try
            {
                gameName = driver.FindElementByClassName("tableName--3PUPn");
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show("Cannot get the game name! Please restart bot!");
            }
            if (gameName == null)
            {
                WriteLog("Cannot get the game name element!");
                return false;
            }
            #endregion

            #region Finding banker
            WriteLog("Finding banker");
            try
            {
                banker = driver.FindElementByClassName("banker--Q8hMb");
                if (banker != null)
                {
                    WriteLog("Banker found");
                }
                else
                {
                    WriteLog($"Banker not found! {restart}");
                    return false;
                }
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show("Please restart bot!");
                return false;
            }
            #endregion

            #region Finding player
            WriteLog("Finding player");
            try
            {
                player = driver.FindElementByClassName("player--1nDyW");
                if (player != null)
                {
                    WriteLog("Player found");
                }
                else
                {
                    WriteLog($"Player not found! {restart}");
                    return false;
                }
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show("Please restart bot!");
                return false;
            }
            #endregion

            #region Finding status
            WriteLog("Finding status");
            try
            {
                status = driver.FindElementByClassName("text--1jzYQ");
                if (status != null)
                {
                    WriteLog("Status found");
                }
                else
                {
                    WriteLog($"Status not found! {restart}");
                    return false;
                }
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            #endregion

            #region Finding balance
            WriteLog("Finding balance");
            try
            {
                balance = driver.FindElementByClassName("amount--3AxPZ");
                if (balance != null)
                {
                    WriteLog("Balance found");
                    ReloadBalance();
                }
                else
                {
                    WriteLog($"Balance not found! {restart}");
                    return false;
                }
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            #endregion

            //amount--3BgvQ
            //title--GvCvW
            #region Finding total bet
            WriteLog("Finding total bet");
            try
            {
                totalBet = driver.FindElementByClassName("amount--3BgvQ");
                if (totalBet != null)
                {
                    WriteLog("Total bet found");
                    ReloadBalance();
                }
                else
                {
                    WriteLog($"totalBet not found! {restart}");
                    return false;
                }
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            #endregion

            #region Tìm label total bet
            WriteLog("Finding total bet");
            try
            {
                labelTotalBet = driver.FindElementByClassName("title--GvCvW");
                if (labelTotalBet != null)
                {
                    WriteLog("labelTotalBet found!");
                    ReloadBalance();
                }
                else
                {
                    WriteLog($"labelTotalBet not found! {restart}");
                    return false;
                }
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            #endregion

            #region Reset collection
            ListCoin.Clear();
            ListCoinCount[0] = 0;
            ListCoinCount[1] = 0;
            ListCoinCount[2] = 0;
            ListCoinCount[3] = 0;
            ListCoinCount[4] = 0;
            ListCoinCount[5] = 0;
            ListCoinCount[6] = 0;
            ListCoinCount[7] = 0;
            ListCoinCount[8] = 0;
            #endregion

            #region Finding coins
            WriteLog("Finding coins");
            try
            {
                ReadOnlyCollection<IWebElement> lstChipClass = null; // đây là element có text là giá tiền
                if (gameName.Text.Equals("Baccarat A") || gameName.Text.Equals("Baccarat B") || gameName.Text.Equals("Baccarat C"))
                {
                    lstChipClass = driver.FindElementsByClassName("chip--1itRi");
                }
                else
                {
                    lstChipClass = driver.FindElementsByClassName("chip--1itRi");
                }

                ListCoin.Add(GetCoinElement(lstChipClass, 1));
                ListCoin.Add(GetCoinElement(lstChipClass, 2));
                ListCoin.Add(GetCoinElement(lstChipClass, 5));
                ListCoin.Add(GetCoinElement(lstChipClass, 25));
                ListCoin.Add(GetCoinElement(lstChipClass, 100));
                ListCoin.Add(GetCoinElement(lstChipClass, 250));
                ListCoin.Add(GetCoinElement(lstChipClass, 500));
                ListCoin.Add(GetCoinElement(lstChipClass, 1000));
                ListCoin.Add(GetCoinElement(lstChipClass, 2500));

                int count = ListCoin.Count(x => x != null);

                if (count != 6)
                {
                    WriteLog($"ListCoin.Count != 6. ListCoin.Count = {count}");
                }

                WriteLog("Got the coins");
            }
            catch (Exception ee)
            {
                if (ee.Message.Contains("stale element reference: element is not attached to the page document"))
                {
                    ReloadButtonStart("Start");
                    return false;
                }
                WriteLog(ee.Message);
                MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            #endregion

            #region Tìm timer
            string messageTimer = "";
            timerElement = SeleniumHelper.FindElement(driver, By.ClassName("timer--1KDVw"), out messageTimer);
            if (!messageTimer.Contains("Found"))
            {
                WriteLog("Cannot find timer!");
                MessageBox.Show("Cannot find timer!");
                return false;
            }
            #endregion

            driver.SwitchTo().DefaultContent();

            return true;
        }
        private void Start(object sender, RoutedEventArgs e)
        {
            if (btnStart.Content.ToString().Equals("Start"))
            {
                btnStart.Content = "Stop";
                t = new Thread(() =>
                {
                    bool isDoneFirstLoadClick = FirstLoadClick();

                    if (ValidateInfo() && isDoneFirstLoadClick)
                    {
                        WriteLog("Game start ");
                        do
                        {
                            SwitchFrame();
                            foreach (var item in ListPlay)
                            {
                                if (item.Value.Equals("Red") || item.Value.Equals("Blue"))
                                {
                                    if (TargetUser == ETargetUser.Admin)
                                    {
                                        WriteLog($"Start betting {item.Value} with {CurrentBet}");
                                    }
                                    foreach (var item2 in ListPlay)
                                    {
                                        item2.IsSelecting = false;
                                        OnPropertyChanged(nameof(item2.IsSelecting));
                                    }
                                    item.IsSelecting = true;
                                    DoABet(item.Value, CurrentBet);
                                }
                                else
                                {
                                    MessageBox.Show("Please logout and restart bot!");
                                    return;
                                }
                                CurrentSuccessBet = 0;
                            }
                            this.Dispatcher.Invoke(() =>
                            {
                                txbLog.Text = "";
                            });
                            driver.SwitchTo().DefaultContent();
                            WriteLog("Done 1 wave!");
                        }
                        while (IsRepeat);
                    }
                    else
                    {

                        this.Dispatcher.Invoke(()=> {
                            btnStart.Content = "Start";
                            CountLoseContinuity = 0;
                            CurrentBet = TienCuoc;
                        });
                        //WriteLog($"{restart}");
                        //MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                });
                t.Start();
            }
            else
            {
                try
                {
                    t.Abort();
                    
                }
                catch (Exception ee)
                {
                    WriteLog(ee.Message);
                }
                btnStart.Content = "Start";
                CountLoseContinuity = 0;
                CurrentBet = TienCuoc;
            }
        }

        private bool ValidateInfo()
        {
            if (TienCuoc <= 0)
            {
                string error = "Betting money must be more than 0";
                MessageBox.Show(error);
                WriteLog(error);
                return false;
            }
            if (ListPlay.Count() != 8)
            {
                string error = "Please logout and restart bot!";
                MessageBox.Show(error);
                WriteLog(error);
                return false;
            }
            foreach (var item in ListPlay)
            {
                if (string.IsNullOrEmpty(item?.Value))
                {
                    string error = "Please logout and restart bot!";
                    MessageBox.Show(error);
                    WriteLog(error);
                    return false;
                }
            }

            return true;
        }
        private void CheckWin3Percent()
        {
            ReloadBalance();
            if (TienBalance >= (TienVon * (1 + StopWhenWinPercent*1.0/100.0)))
            {
                // lời quá 3%
                WriteLog($"Interst at {StopWhenWinPercent}%. Don’t turn investment into a game of chance. Thank you! Start money: {TienVon}. Balance: {TienBalance}");
                MessageBox.Show("Interst at {StopWhenWinPercent}%. Don’t turn investment into a game of chance. Thank you!", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                ReloadButtonStart("Start"); Thread.CurrentThread.Abort();
                return;
            }
        }
        private void DoABet(string selection, decimal money)
        {
            CurrentSuccessBet = 0;
            TotalBetMoney = 0;
            bool isSuccess = false;
            // check tiền bet
            WriteLog("Check balance before bet");
            ReloadBalance();

            // validate checking
            #region Validate checking
            // check còn đủ tiền đánh không
            for (int i = 0; i < 9; i++)
            {
                ReloadBalance();
                if (TienBalance < Convert.ToDouble(money))
                {
                    WriteLog($"Times: {i + 1}. Not enough money. Balance: {TienBalance}. Needed money: {money}");
                    Thread.Sleep(TimeSpan.FromSeconds(0.3));
                }
                else
                {
                    break;
                }
            }
            ReloadBalance();
            if (TienBalance < Convert.ToDouble(money))
            {
                WriteLog($"Not enough money. Balance: {TienBalance}. Needed money: {money}");
                MessageBox.Show($"Not enough money. Balance: {TienBalance}. Needed money: {money}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                ReloadButtonStart("Start");
                Thread.CurrentThread.Abort();
                return;
            }
            // check xem lời quá 3% thì dừng
            if (IsStopWhenWin)
            {
                CheckWin3Percent();
            }
            // check xem thua quá số lần quy định chưa
            if (IsDouble && (CountLoseContinuity >= LoseContinuity))
            {
                MessageBox.Show($"Lose more than {LoseContinuity} times. Please restart your game!");
                WriteLog($"Lose more than {LoseContinuity} times. Please restart your game!");
                CurrentBet = TienCuoc;
                ReloadButtonStart("Start"); Thread.CurrentThread.Abort();
                return;
            }
            #endregion

            // chờ đến khi được bet
            try
            {
                string oldStatus = status.Text;
                while (!status.Text.Contains("PLACE YOUR BETS"))
                {
                    if (!(oldStatus == status.Text))
                    {
                        WriteLog($"Wait for your turn {status.Text}");
                        oldStatus = status.Text;
                    }
                    Thread.Sleep(TimeSpan.FromSeconds(0.1));
                }
            }
            catch (Exception ee)
            {
                if (ee.Message.Contains("stale element reference: element is not attached to the page document"))
                {
                    ReloadButtonStart("Start");
                }
            }
            
            WriteLog($"Allow to bet {status.Text}");

            #region Check timer > 7s
            Thread.Sleep(TimeSpan.FromSeconds(1));
            try
            {
                if (Convert.ToInt32(timerElement.Text.ToString()) <= 7)
                {
                    WriteLog($"Time is not enough!");
                    MessageBox.Show($"Time is not enough! Please restart your game!");
                    WriteLog($"Time is not enough! Please restart your game!");
                    ReloadButtonStart("Start"); Thread.CurrentThread.Abort();
                }
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                Thread.Sleep(TimeSpan.FromSeconds(8));
                return;
            }

            #endregion

            #region Set totalbet = 0
            //WriteLog("Reset totalbet = 0");
            //string jsString = $"document.getElementsByClassName('amount--3BgvQ')[0].innerHTML=\"$ {0}\";";
            //// thực thi JavaScript dùng IJavaScriptExecutor
            //IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            //// javascript cần return giá trị.
            //var dataFromJS = (string)js.ExecuteScript(jsString);
            #endregion

            #region Click betting
            if (TargetUser == ETargetUser.Admin)
            {
                WriteLog($"Assigning {selection}"); // banker
            }

            IWebElement betObject = selection.Equals("Red") ? banker : player;
            if (betObject != null)
            {
                WriteLog("Bet object found");
                ClickBetting(betObject, money);
                WriteLog("Finish bet");
            }
            else
            {
                WriteLog("betObject not found");
                return;
            }
            #endregion


            ReloadBalance();

            try
            {
                string oldStatus = status.Text;
                while (!(status.Text.Contains("WINS") || status.Text.StartsWith("TIE")))
                {
                    if (oldStatus != status.Text)
                    {
                        WriteLog($"Waiting for winner. Status bar = {status.Text}");
                        oldStatus = status.Text;
                    }
                    if (status.Text.Contains("BETS ACCEPTED"))
                    {
                        isSuccess = true;

                        if(oldStatus != status.Text)
                        {
                            WriteLog($"{status.Text}");
                            oldStatus = status.Text;
                        }                       
                    }
                    Thread.Sleep(TimeSpan.FromSeconds(0.1));
                }
            }
            catch (Exception ee)
            {
                if (ee.Message.Contains("stale element reference: element is not attached to the page document"))
                {
                    ReloadButtonStart("Start");
                }
            }           

            if (isSuccess)
            {
                string winner = status.Text.Split(new string[] { "WINS" }, StringSplitOptions.None).FirstOrDefault().Trim();
                if (winner.Contains("BANKER") || winner.Contains("PLAYER"))
                {
                    WriteLog($"The winner is {winner}! Starting a new bet");
                    // selection = đỏ = banker
                    // selection = xanh = player
                    if ((winner.Equals("BANKER") && selection.Equals("Red")) || (winner.Equals("PLAYER") && selection.Equals("Blue")))
                    {
                        ++Win;
                        CurrentBet = TienCuoc;
                        CountLoseContinuity = 0;
                        WriteLog($"Win. CountLose: {CountLoseContinuity}");
                    }
                    else
                    {
                        ++Lose;
                        ++CountLoseContinuity;
                        WriteLog($"Lose. CountLose: {CountLoseContinuity}");
                        if (IsDouble)
                        {
                            if (CountLoseContinuity < LoseContinuity)
                            {
                                CurrentBet = CurrentBet * 2;
                            }
                            else
                            {
                                MessageBox.Show($"Lose more than {LoseContinuity} times. Please restart your game!");
                                CurrentBet = TienCuoc;
                                ReloadButtonStart("Start"); Thread.CurrentThread.Abort();
                                return;
                            }
                        }
                    }
                }
                else if (winner.Contains("TIE"))
                {
                    WriteLog("TIE! Starting a new bet");
                    ++Tie;
                }
            }
            else
            {
                WriteLog($"Betting failed!");
                // quay lại số tiền ban đầu
                CurrentBet = TienCuoc;
                CountLoseContinuity = 0;
            }

            ReloadBalance();
        }
        private void ReloadButtonStart(string text)
        {
            this.Dispatcher.Invoke(() =>
            {
                btnStart.Content = text;
                if (text == "Start")
                {
                    CountLoseContinuity = 0;
                    CurrentBet = TienCuoc;
                }
            });
            try
            {
                Thread.CurrentThread.Abort();
            }
            catch (Exception ee)
            {

            }
        }
        private void OpenChromeDriver()
        {
            if (driver != null)
            {
                try
                {
                    driver.Close();
                    driver.Quit();
                }
                catch (Exception ex)
                {
                }
            }
            ChromeOptions options = new ChromeOptions();

            if (!Directory.Exists(ProfileFolderPath))
            {
                Directory.CreateDirectory(ProfileFolderPath);
            }
            if (Directory.Exists(ProfileFolderPath))
            {
                options.AddArguments("user-data-dir=" + ProfileFolderPath);
            }
            options.AddArgument($"--user-agent={LoadUserAgent()}");
            //options.AddArgument("--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36");
            options.AddExcludedArgument("enable-automation");
            options.AddAdditionalCapability("useAutomationExtension", false);
            options.AddArguments("--disable-blink-features=AutomationControlled");
#if DEBUG
            driver = new ChromeDriver(options);
            //driver = new ChromeDriver(@"D:\NA\Downloads\chromedriver_win32", options);
#else
            driver = new ChromeDriver(options);
            //driver = new ChromeDriver(@"D:\NA\Downloads\chromedriver_win32", options);
#endif
            driver.Url = $"https://{WebName}";
            driver.Navigate();
        }
        private void ReloadBalance()
        {
            if (balance == null)
            {
                WriteLog("Balance element not found!");
                return;
            }
            else
            {
                string balanceText = Regex.Replace(balance.Text, "[^0-9.]", "");
                WriteLog($"Balance text {balanceText}");
                if (!string.IsNullOrEmpty(balanceText))
                {
                    TienBalance = Convert.ToDouble(balanceText, CultureInfo.GetCultureInfo("En"));
                    OnPropertyChanged(nameof(TienBalance));
                    WriteLog(TienBalance.ToString());
                    CalculateProfit();
                }
            }
        }

        void CalculateProfit()
        {
            Profit = TienBalance - TienVon;
        }
        private bool LoadWayBetting(Dictionary<int, int[]> listWay, int wayIndex, decimal money)
        {
            try
            {
                if (listWay[(int)money] == null)
                {
                    WriteLog("List gameplay has error! Please check and restart your bot!");
                    MessageBox.Show($"List gameplay has error! Please check and restart your bot!");
                    ReloadButtonStart("Start");
                    return false;
                }
            }
            catch (Exception ee)
            {
                WriteLog("List gameplay has error! Please check and restart your bot!");
                MessageBox.Show($"List gameplay has error! Please check and restart your bot!");
                ReloadButtonStart("Start");
                return false;
            }

            switch (wayIndex)
            {
                case 1:
                    ListCoinCount[0] = ListWay1[(int)money].ElementAt(0);
                    ListCoinCount[1] = 0;
                    ListCoinCount[2] = ListWay1[(int)money].ElementAt(1);
                    ListCoinCount[3] = ListWay1[(int)money].ElementAt(2);
                    ListCoinCount[4] = ListWay1[(int)money].ElementAt(3);
                    ListCoinCount[5] = ListWay1[(int)money].ElementAt(4);
                    ListCoinCount[6] = ListWay1[(int)money].ElementAt(5);
                    ListCoinCount[7] = 0;
                    ListCoinCount[8] = 0;
                    break;
                case 3:
                    ListCoinCount[0] = ListWay3[(int)money].ElementAt(0);
                    ListCoinCount[1] = 0;
                    ListCoinCount[2] = ListWay3[(int)money].ElementAt(1);
                    ListCoinCount[3] = ListWay3[(int)money].ElementAt(2);
                    ListCoinCount[4] = ListWay3[(int)money].ElementAt(3);
                    ListCoinCount[5] = 0;
                    ListCoinCount[6] = ListWay3[(int)money].ElementAt(4);
                    ListCoinCount[7] = 0;
                    ListCoinCount[8] = ListWay3[(int)money].ElementAt(5);
                    break;
                case 4:
                    ListCoinCount[0] = ListWay4[(int)money].ElementAt(0);
                    ListCoinCount[1] = 0;
                    ListCoinCount[2] = ListWay4[(int)money].ElementAt(1);
                    ListCoinCount[3] = ListWay4[(int)money].ElementAt(2);
                    ListCoinCount[4] = ListWay4[(int)money].ElementAt(3);
                    ListCoinCount[5] = 0;
                    ListCoinCount[6] = 0;
                    ListCoinCount[7] = ListWay4[(int)money].ElementAt(4);
                    ListCoinCount[8] = ListWay4[(int)money].ElementAt(5);
                    break;
                case 6:
                    ListCoinCount[0] = 0;
                    ListCoinCount[1] = ListWay6[(int)money].ElementAt(0);
                    ListCoinCount[2] = ListWay6[(int)money].ElementAt(1);
                    ListCoinCount[3] = ListWay6[(int)money].ElementAt(2);
                    ListCoinCount[4] = ListWay6[(int)money].ElementAt(3);
                    ListCoinCount[5] = 0;
                    ListCoinCount[6] = ListWay6[(int)money].ElementAt(4);
                    ListCoinCount[7] = 0;
                    ListCoinCount[8] = ListWay6[(int)money].ElementAt(5);
                    break;
                case 7:
                    ListCoinCount[0] = 0;
                    ListCoinCount[1] = ListWay7[(int)money].ElementAt(0);
                    ListCoinCount[2] = ListWay7[(int)money].ElementAt(1);
                    ListCoinCount[3] = ListWay7[(int)money].ElementAt(2);
                    ListCoinCount[4] = ListWay7[(int)money].ElementAt(3);
                    ListCoinCount[5] = 0;
                    ListCoinCount[6] = 0;
                    ListCoinCount[7] = ListWay7[(int)money].ElementAt(4);
                    ListCoinCount[8] = ListWay7[(int)money].ElementAt(5);
                    break;
                case 8:
                    ListCoinCount[0] = ListWay8[(int)money].ElementAt(0);
                    ListCoinCount[1] = 0;
                    ListCoinCount[2] = ListWay8[(int)money].ElementAt(1);
                    ListCoinCount[3] = ListWay8[(int)money].ElementAt(2);
                    ListCoinCount[4] = ListWay8[(int)money].ElementAt(3);
                    ListCoinCount[5] = 0;
                    ListCoinCount[6] = ListWay8[(int)money].ElementAt(4);
                    ListCoinCount[7] = ListWay8[(int)money].ElementAt(5);
                    ListCoinCount[8] = 0;
                    break;
                default:
                    WriteLog($"Listways doesn't exist! wayIndex = {wayIndex}");
                    MessageBox.Show($"List gameplay has error! Please check and restart your bot!");
                    ReloadButtonStart("Start");
                    return false;
            }

            return true;
        }
        private void ClickBetting(IWebElement betObject, decimal money)
        {
            if (betObject != null && money > 0)
            {
                // Tính toán cái gì đó để ra ListCoinCount
                if (WebName == "bitcasino.io")
                {
                    switch (gameName.Text)
                    {
                        case "Baccarat A":
                        case "Baccarat B":
                        case "Baccarat C":
                            //ListWay1
                            if (LoadWayBetting(ListWay1, 1, money))
                            {
                                WriteLog($"Load thành công ListWay1");
                            }
                            break;
                        case "Speed Baccarat A":
                        case "Speed Baccarat B":
                        case "Speed Baccarat C":
                        case "Speed Baccarat D":
                        case "Speed Baccarat E":
                        case "Punto Banco":
                        case "Korean Speed Baccarat A":
                            // ListWay3
                            if (LoadWayBetting(ListWay3, 3, money))
                            {
                                WriteLog($"Load thành công ListWay3");
                            }
                            break;
                        case "Bombay Club Japanese Speed Baccarat":
                        case "Bombay Club Japanese Speed Baccarat 2":
                        case "Bombay Club Speed Baccarat 1":
                            //ListWay4
                            if (LoadWayBetting(ListWay4, 4, money))
                            {
                                WriteLog($"Load thành công ListWay4");
                            }
                            break;
                        case "Speed Baccarat F":
                        case "Speed Baccarat G":
                        case "Speed Baccarat H":
                        case "Speed Baccarat I":
                        case "Speed Baccarat J":
                        case "Speed Baccarat K":
                        case "Speed Baccarat L":
                            // ListWay6
                            if (LoadWayBetting(ListWay6, 6, money))
                            {
                                WriteLog($"Load thành công ListWay6");
                            }
                            break;
                        case "Bombay Thai Speed Baccarat":
                        case "Bombay Club Speed Baccarat 4":
                            //ListWay7
                            if (LoadWayBetting(ListWay7, 7, money))
                            {
                                WriteLog($"Load thành công ListWay7");
                            }
                            break;
                        default:
                            WriteLog("Something went wrong with gameName");
                            return;
                    }
                }
                else if (WebName == "southqueenpro.com" || WebName == "bigplayerclub.com")
                {
                    if (LoadWayBetting(ListWay8, 8, money))
                    {
                        WriteLog($"Load thành công ListWay8");
                    }
                }
                else if (WebName == "studio.evolutiongaming.com")
                {
                    if (LoadWayBetting(ListWay3, 3, money))
                    {
                        WriteLog($"Load thành công ListWay3");
                    }
                }

                WriteLog($"Gameplay: {ListCoinCount[0]} {ListCoinCount[1]} {ListCoinCount[2]} {ListCoinCount[3]} {ListCoinCount[4]} {ListCoinCount[5]} {ListCoinCount[6]} {ListCoinCount[7]} {ListCoinCount[8]}");

                WriteLog($"Start betting with {money}");
                // Click 1 ván đầu
                for (int i = 0; i < ListCoin.Count(); i++)
                {
                    try
                    {
                        if (ListCoin.ElementAt(i) != null)
                        {
                            ClickACoinBet(betObject, ListCoin.ElementAt(i), ListCoinCount.ElementAt(i));
                        }
                    }
                    catch (Exception ee)
                    {
                        WriteLog(ee.Message);
                        MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                        ReloadButtonStart("Start"); Thread.CurrentThread.Abort();
                    }
                }
                //Check xem có cần double không thì nhấn double
                //if (CountLoseContinuity != 0)
                //{
                //    #region Tìm double
                //    var lstButtonDouble = driver.FindElementsByClassName("button--3h5xe");
                //    try
                //    {
                //        doubleElement = lstButtonDouble.FirstOrDefault((p) => p.GetAttribute("data-role").Equals("double-button"));
                //        if (doubleElement != null)
                //        {
                //            WriteLog("Tìm được double element");
                //        }
                //        else
                //        {
                //            WriteLog("Không tìm được double element");
                //            MessageBox.Show($"{restart}","Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                //            ReloadButtonStart("Start"); Thread.CurrentThread.Abort(); 
                //        }
                //    }
                //    catch (Exception ee)
                //    {
                //        WriteLog(ee.Message);
                //        MessageBox.Show($"{restart}","Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                //        ReloadButtonStart("Start"); Thread.CurrentThread.Abort(); 
                //    }
                //    #endregion
                //}
                //for (int i = 0; i < CountLoseContinuity; i++)
                //{
                //    ClickDouble();
                //}

                #region Nháp
                //int soto = 0;
                //int[] sotoCollection = new int[] { };
                //if (!isHave1)
                //{
                //    sotoCollection = CalculateMoneyWith2(money);
                //    if (sotoCollection == null || sotoCollection.Count() != tien.Length)
                //    {
                //        string error = "Vui lòng thoát ra và đăng nhập lại!";
                //        MessageBox.Show(error);
                //        WriteLog(error);
                //        ReloadButtonStart("Start"); Thread.CurrentThread.Abort(); 
                //        return;
                //    }
                //}

                //for (int i = 0; i < tien.Length; i++)
                //{
                //    soto = (isHave1) ? Convert.ToInt32(Math.Floor(money / tien[i])) : sotoCollection[i];
                //    WriteLog($"Số tiền: {tien[i]}. Số tờ: {soto}");
                //    if (soto > 0)
                //    {
                //        WriteLog($"money = {tien[i]}, soto = {soto}");
                //        ClickABet(betObject, FindAMoneyElement(tien[i]), soto);
                //    }
                //    money = (isHave1) ? money % tien[i] : money;
                //    //WriteLog($"Số tiền còn lại: {money}");
                //}
                #endregion
            }
            else
            {
                MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                WriteLog($"{restart}");
                ReloadButtonStart("Start"); Thread.CurrentThread.Abort();
            }
        }
        private void ClickACoinBet(IWebElement betObject, IWebElement coinElement, int count)
        {
            if (count > 0)
            {
                try
                {
                    decimal coinDataValue = Convert.ToDecimal(coinElement.GetAttribute("data-value"));
                    decimal neededMoney = count * coinDataValue;
                    WriteLog($"Money need to click: {neededMoney}");

                    #region Click money
                    while (!coinElement.Displayed)
                    {
                        WriteLog($"Waiting for {coinElement.Text}$ displayed");
                        Thread.Sleep(TimeSpan.FromSeconds(0.02));
                    }
                    string jsClickCoin = $"(Array.from(document.getElementsByClassName('chip--1itRi')).filter(field => field.getAttribute('data-value') == {coinDataValue})[0]).click()";
                    IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                    string res = (string)js.ExecuteScript(jsClickCoin);
                    //coinElement.FindElement(By.XPath("..")).FindElement(By.XPath("..")).Click();
                    WriteLog($"Done clicking {coinElement.Text}$");
                    #endregion


                    for (int i = 0; i < count; i++)
                    {
                        ClickBetObject(betObject, js);
                        //decimal totalBetOld = Convert.ToDecimal(totalBet.Text.Replace("$", "").Trim());
                        //betObject.Click();
                        //Thread.Sleep(TimeSpan.FromSeconds(0.3));
                        //while (Convert.ToDecimal(totalBet.Text.Replace("$", "").Trim()) == totalBetOld/* && labelTotalBet.Text.Contains("TOTAL BET")*/) 
                        //{
                        //    WriteLog($"Failed to click. TotalBetOld = {totalBetOld}. TotalBetCurrent = {Convert.ToDecimal(totalBet.Text.Replace("$", "").Trim())}");
                        //    betObject.Click();
                        //    Thread.Sleep(TimeSpan.FromSeconds(0.3));
                        //}
                        WriteLog($"Click betobject");
                        //WriteLog($"Tiền totalbet sau bet: {totalBet.Text.Replace("$", "").Trim()}");
                        //WriteLog("------------");
                    }
                    WriteLog($"{TotalBetMoney}");
                    TotalBetMoney = Convert.ToDecimal(totalBet.Text.Split(' ').LastOrDefault().Trim());
                    while (Math.Abs(TotalBetMoney - CurrentSuccessBet) < neededMoney)
                    {
                        WriteLog($"Reclick");
                        //betObject.Click();
                        ClickBetObject(betObject, js);
                        WriteLog($"Click betobject");
                        TotalBetMoney = Convert.ToDecimal(totalBet.Text.Split(' ').LastOrDefault().Trim());
                    }

                    TotalBetMoney = Convert.ToDecimal(totalBet.Text.Split(' ').LastOrDefault().Trim());
                    CurrentSuccessBet = Convert.ToDecimal(totalBet.Text.Split(' ').LastOrDefault().Trim());
                }
                catch (Exception ee)
                {
                    WriteLog(ee.Message);
                    MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ReloadButtonStart("Start"); Thread.CurrentThread.Abort();
                }
            }
        }
        private string ClickBetObject(IWebElement betObject, IJavaScriptExecutor js)
        {
            if (betObject.Text.Contains("BANKER"))
            {
                string resBanker = (string)js.ExecuteScript(jsClickBanker);
                WriteLog($"{resBanker}");
                return resBanker;
            }
            else if (betObject.Text.Contains("PLAYER"))
            {
                string resPlayer = (string)js.ExecuteScript(jsClickPlayer);
                WriteLog($"{resPlayer}");
                return resPlayer;
            }
            return "";
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            ExitApp();
        }
        private void ExitApp()
        {
            try
            {
                if (constants.IsCheckServer)
                {
                    timer.Stop();
                }
                this.Connection.Stop();
                driver?.Close();
                driver?.Quit();
                this.Close();
                System.Windows.Application.Current.ShutdownMode = ShutdownMode.OnLastWindowClose;
                System.Windows.Application.Current.Shutdown();
            }
            catch (Exception ee)
            {
            }
        }






        private void ClickDouble()
        {
            try
            {
                if (doubleElement != null)
                {
                    while (!doubleElement.Displayed)
                    {
                        WriteLog($"Wait until {doubleElement.Text}$ shows");
                        Thread.Sleep(TimeSpan.FromSeconds(0.1));
                    }
                    doubleElement.Click();
                    WriteLog("Done clicking double");
                }
                else
                {
                    WriteLog("Không thấy button double!");
                    MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ReloadButtonStart("Start"); Thread.CurrentThread.Abort();
                }
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
                MessageBox.Show($"{restart}", "Alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                ReloadButtonStart("Start"); Thread.CurrentThread.Abort();
            }
        }
        private void ClickABet(IWebElement betObject, IWebElement moneyElement, decimal count)
        {
            if (betObject != null && moneyElement != null && count >= 0)
            {
                // Click money
                try
                {
                    while (!moneyElement.Displayed)
                    {
                        WriteLog($"Chờ đến khi đồng {moneyElement.Text}$ hiển thị");
                        Thread.Sleep(TimeSpan.FromSeconds(0.1));
                    }
                    moneyElement.FindElement(By.XPath("..")).FindElement(By.XPath("..")).Click();
                    WriteLog($"Done clicking {moneyElement.Text}$");
                }
                catch (Exception ee)
                {
                    WriteLog(ee.Message);
                    WriteLog("Phần chơi hiện tại sẽ bị hủy!");
                    ReloadButtonStart("Start"); Thread.CurrentThread.Abort();
                }

                // Click bet
                for (int i = 0; i < count; i++)
                {
                    try
                    {
                        betObject.Click();
                    }
                    catch (Exception ee)
                    {
                        WriteLog(ee.Message);
                        WriteLog("Phần chơi hiện tại sẽ bị hủy!");
                        ReloadButtonStart("Start"); Thread.CurrentThread.Abort();
                    }
                }
            }
            else
            {
                MessageBox.Show("Vui lòng thoát trình duyệt và start lại.");
                WriteLog("Vui lòng thoát trình duyệt và start lại.");
            }
        }
        private void ClickDouble(int count)
        {
            var lstButtonDouble = driver.FindElementsByClassName("button--3h5xe");
            IWebElement btnDouble = null;
            try
            {
                btnDouble = lstButtonDouble.FirstOrDefault((p) => p.GetAttribute("data-role").Equals("double-button"));
            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
            }
            if (btnDouble == null)
            {
                WriteLog($"Không tìm được btnDouble");
            }
            else
            {
                WriteLog($"Tìm được btnDouble");
                for (int i = 0; i < count; i++)
                {
                    try
                    {
                        btnDouble.Click();
                    }
                    catch (Exception ee)
                    {
                        WriteLog(ee.Message);
                    }
                }
            }
        }
        private IWebElement FindAMoneyElement(decimal moneyValue)
        {
            IWebElement moneyElement = null;

            WriteLog($"Bắt đầu tìm ô {moneyValue}");
            try
            {
                var lstChipClass = driver.FindElementsByClassName("chip--1itRi"); // đây là element có text là giá tiền
                moneyElement = lstChipClass.FirstOrDefault((p) => p.GetAttribute("data-role").Equals("chip") && p.GetAttribute("data-value").Equals($"{moneyValue}"));

            }
            catch (Exception ee)
            {
                WriteLog(ee.Message);
            }

            if (moneyElement != null)
            {
                WriteLog($"Tìm được moneyElement {moneyValue}$");
            }
            else
            {
                WriteLog($"Không tìm được moneyElement {moneyValue}$");
            }

            return moneyElement;
        }


    }
}