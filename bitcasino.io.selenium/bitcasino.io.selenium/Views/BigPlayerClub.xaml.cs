﻿using bitcasino.io.selenium.Helpers;
using bitcasino.io.selenium.Models;
using Microsoft.AspNet.SignalR.Client;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static bitcasino.io.selenium.Utilities.BettingUtilities;
using static bitcasino.io.selenium.Views.BotWindow;

namespace bitcasino.io.selenium.Views
{
    /// <summary>
    /// Interaction logic for BigPlayerClub.xaml
    /// </summary>
    public partial class BigPlayerClub : Window, INotifyPropertyChanged
    {
        #region Helpers
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        private void WriteLog(string log)
        {
            this.Dispatcher.Invoke(new Action(() => {
                txbLog.Text = log + Environment.NewLine + txbLog.Text;
            }));

            #region Write to file
            string path = "Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = "Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!System.IO.File.Exists(filepath))
            {
                // Create a file to write to.
                using (StreamWriter sw = System.IO.File.CreateText(filepath))
                {
                    sw.WriteLine(log);
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(filepath))
                {
                    sw.WriteLine(log);
                }
            }

            #endregion
        }
        #endregion

        #region Properties
        string restart = "Unstable connection! Please check your connection and restart your BOT!";
        private ChromeDriver driver;
        string ProfileFolderPath = "Profile";
        public IHubProxy Proxy { get; set; }
        public HubConnection Connection { get; set; }

        private bool _Admin;
        public bool Admin
        {
            get => _Admin;
            set
            {
                _Admin = value;
                if (Admin)
                {
                    IsAdmin = Visibility.Visible;
                    IsNotAdmin = Visibility.Hidden;
                }
                else
                {
                    IsAdmin = Visibility.Hidden;
                    IsNotAdmin = Visibility.Visible;
                }

            }
        }

        private UserAccountInfo _Account;
        public UserAccountInfo Account { get => _Account; set { _Account = value; OnPropertyChanged(); } }

        private Visibility _IsAdmin = Visibility.Visible;
        public Visibility IsAdmin { get => _IsAdmin; set { _IsAdmin = value; OnPropertyChanged(); } }

        private Visibility _IsNotAdmin = Visibility.Hidden;
        public Visibility IsNotAdmin { get => _IsNotAdmin; set { _IsNotAdmin = value; OnPropertyChanged(); } }

        private ETargetUser _TargetUser = ETargetUser.Admin;
        public ETargetUser TargetUser { get => _TargetUser; set { _TargetUser = value; OnPropertyChanged(); } }

        private Accs _Accs;
        public Accs Accs { get => _Accs; set { _Accs = value; OnPropertyChanged(); } }

        private ObservableCollection<BetItem> _ListPlay = new ObservableCollection<BetItem>();
        public ObservableCollection<BetItem> ListPlay { get => _ListPlay; set { _ListPlay = value; OnPropertyChanged(); } }

        private ObservableCollection<BetItem> _ListSelection = new ObservableCollection<BetItem>() { new BetItem { Value = "Đỏ", IsSelecting = false }, new BetItem { Value = "Xanh", IsSelecting = false } };
        public ObservableCollection<BetItem> ListSelection { get => _ListSelection; set { _ListSelection = value; OnPropertyChanged(); } }

        private bool _IsRepeat;
        public bool IsRepeat { get => _IsRepeat; set { _IsRepeat = value; OnPropertyChanged(); } }

        private bool _IsDouble;
        public bool IsDouble { get => _IsDouble; set { _IsDouble = value; OnPropertyChanged(); } }
        #endregion

        #region Game element
        IWebElement banker = null;
        IWebElement player = null;
        IWebElement status = null;
        IWebElement balance = null;
        IWebElement tienVon = null;
        IWebElement doubleElement = null;
        IWebElement totalBet = null;
        IWebElement labelTotalBet = null;
        IWebElement gameName = null;
        #endregion



        public BigPlayerClub()
        {
            InitializeComponent();
            DataContext = this;

            Admin = _TargetUser == ETargetUser.Admin;

            FirstLoad();
        }
        private void FirstLoad()
        {
            if (constants.IsCheckServer)
            {
                Account = constants.Account;
                Accs = constants.CurrentAccs;
            }
            //WriteLog("Vui lòng đăng nhập vào game theo thông tin bên dưới!");
            //WriteLog($"Accs.Password: {Accs.PasswordRaw}");
            //WriteLog($"Accs.Username: {Accs.Email}");

            #region Init bot selection
            WriteLog("Creating calculation");
            ListPlay.Add(ListSelection.FirstOrDefault());
            ListPlay.Add(ListSelection.FirstOrDefault());
            ListPlay.Add(ListSelection.FirstOrDefault());
            ListPlay.Add(ListSelection.FirstOrDefault());
            ListPlay.Add(ListSelection.LastOrDefault());
            ListPlay.Add(ListSelection.LastOrDefault());
            ListPlay.Add(ListSelection.FirstOrDefault());
            ListPlay.Add(ListSelection.LastOrDefault());

            IsRepeat = true; IsDouble = true;
            WriteLog("Calculation created");
            #endregion

            //LoadWays();

            //if (constants.IsCheckServer)
            //{
            //    ConnectSignalR();
            //    timer = new DispatcherTimer();
            //    timer.Interval = TimeSpan.FromSeconds(1);
            //    timer.Tick += timer_Tick;
            //    timer.Start();
            //}

            //OpenChromeDriver();
        }
    }
}
