﻿using bitcasino.io.web.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace bitcasino.io.web.Models
{
    public class CreateUserBindingModel
    {
        public string Id { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "{0} không được để trống")]
        [StringLength(256, ErrorMessage = "{0} tối đa {1} ký tự")]
        public string Email { get; set; }

        [Display(Name = "Phone")]
        [Required(ErrorMessage = "{0} không được để trống")]
        [StringLength(256, ErrorMessage = "{0} tối đa {1} ký tự")]
        public string Phone { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "{0} không được để trống")]
        [StringLength(256, ErrorMessage = "{0} tối đa {1} ký tự")]
        public string Password { get; set; }

        [Display(Name = "Ngày bắt đầu")]
        [Required(ErrorMessage = "{0} không được để trống")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyy}")]
        public DateTime ActiveDate { get; set; }

        [Display(Name = "Thời gian sử dụng (tính bằng ngày)")]
        [RequiredIf("RoleId", "3", ErrorMessage = "{0} không được để trống")]
        [Range(30, 360, ErrorMessage = "{0} phải nằm giữa {1} và {2}")]
        public byte? Duration { get; set; }

        [Display(Name = "Role")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public string RoleId { get; set; }
    }
}