﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace bitcasino.io.web.Models
{
    public class DeleteAccViewModel
    {
        [Required(ErrorMessage = "{0} không được để trống")]
        public int AccId { get; set; }

        [Required(ErrorMessage = "{0} không được để trống")]
        public string Password { get; set; }
    }
}