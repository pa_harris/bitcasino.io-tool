﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace bitcasino.io.web.Models
{
    public class CreateAccsBindingModel
    {
        public int Id { get; set; }

        [Display(Name = "UserName")]
        [Required(ErrorMessage = "{0} không được để trống")]
        [StringLength(256, ErrorMessage = "{0} tối đa {1} ký tự")]
        public string Email { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "{0} không được để trống")]
        [StringLength(256, ErrorMessage = "{0} tối đa {1} ký tự")]
        public string PasswordRaw { get; set; }

        [Display(Name = "Trạng thái")]
        [Required(ErrorMessage = "{0} không được để trống")]
        public Nullable<bool> IsUsing { get; set; }
    }
}