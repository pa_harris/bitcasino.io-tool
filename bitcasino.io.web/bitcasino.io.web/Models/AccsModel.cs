﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bitcasino.io.web.Models
{
    public class AccsModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string PasswordRaw { get; set; }
        public bool IsUsing { get; set; }
    }
}
