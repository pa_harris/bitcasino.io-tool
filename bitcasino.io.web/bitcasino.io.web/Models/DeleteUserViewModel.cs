﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace bitcasino.io.web.Models
{
    public class DeleteUserViewModel
    {
        [Required(ErrorMessage = "{0} không được để trống")]
        public string UserId { get; set; }

        [Required(ErrorMessage = "{0} không được để trống")]
        public string Password { get; set; }
    }
}