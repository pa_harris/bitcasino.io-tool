﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bitcasino.io.web.Models
{
    public class PageList
    {
        public int PageNum { get; set; }
        public int PageSize { get; set; }
        public long Total { get; set; }
    }
}
