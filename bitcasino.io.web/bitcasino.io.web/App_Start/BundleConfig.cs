﻿using System.Web;
using System.Web.Optimization;

namespace bitcasino.io.web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/umd/popper.js",
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/styles").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/fontawesome/css/all.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-ui").Include(
                        "~/Scripts/jquery-ui-{version}.js",
                        "~/Scripts/jquery-ui.datepicker-vi-VN.js",
                         "~/Plugins/jquery-timepicker/jquery.timepicker.min.js"));

            bundles.Add(new StyleBundle("~/Content/jquery-ui").Include(
                      "~/Content/themes/base/jquery-ui.css",
                      "~/Plugins/jquery-timepicker/jquery.timepicker.min.css"));

            bundles.Add(new StyleBundle("~/Content/common-styles").Include(
                    "~/Content/bootstrap.css",
                    "~/Content/fontawesome/css/all.css"));

            bundles.Add(new ScriptBundle("~/bundles/common-libs").Include(
                    "~/Scripts/jquery-{version}.js",
                    "~/Scripts/umd/popper.js",
                    "~/Scripts/bootstrap.js",
                    "~/Scripts/toastr.js",
                    "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/main/js").Include(
                    "~/Scripts/main.js"));

            bundles.Add(new StyleBundle("~/Content/sb-admin").Include(
                      "~/App_Themes/SB_Admin_2/css/sb-admin-2.css",
                      "~/Content/toastr.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/sb-admin").Include(
                        "~/App_Themes/SB_Admin_2/js/sb-admin-2.js"));

            #region data table

            bundles.Add(new StyleBundle("~/Content/datatables").Include(
                      "~/Plugins/datatables/dataTables.bootstrap4.css"));

            bundles.Add(new ScriptBundle("~/bundles/datatables").Include(
                        "~/Plugins/datatables/jquery.dataTables.js",
                        "~/Plugins/datatables/dataTables.bootstrap4.js"));

            #endregion

            #region ckeditor
            bundles.Add(new ScriptBundle("~/js/cke").Include(
                        "~/Plugins/ckeditor/ckeditor.js"));
            bundles.Add(new ScriptBundle("~/js/ckadapter").Include(
                        "~/Plugins/ckeditor/adapters/jquery.js"));
            #endregion

#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
            BundleTable.EnableOptimizations = true;
#endif
        }
    }
}
