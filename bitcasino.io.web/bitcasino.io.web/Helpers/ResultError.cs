﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace bitcasino.io.web.Helpers
{
    public class ResultError
    {
        public string Pos { get; set; }
        public string Error { get; set; }

        public static void GetListErrors(ModelStateDictionary ModelState, ref List<ResultError> errors)
        {
            foreach (var vl in ModelState)
            {
                if (vl.Value.Errors.Count > 0)
                {
                    var er = new ResultError();
                    er.Pos = vl.Key;
                    foreach (var err in vl.Value.Errors)
                    {
                        er.Error = err.ErrorMessage;
                        errors.Add(er);
                        break;
                    }
                }
            }
        }
    }
    public enum ErrorCode : int
    {
        ErrorMess = -2,
        ErrorList = -1,
        Success = 1
    }

    public enum ResponseCode : int
    {
        Error = -1,
        Success = 1,
        NotFound = 404,
    }
}