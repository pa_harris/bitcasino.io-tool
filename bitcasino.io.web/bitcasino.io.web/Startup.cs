﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(bitcasino.io.web.Startup))]
namespace bitcasino.io.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
