﻿// init ckeditor base path first
CKEDITOR_BASEPATH = "/Plugins/ckeditor/";

(function timeAgo(selector) {
    var templates = {
        prefix: "",
        suffix: " trước",
        seconds: "vài giây",
        minute: "khoảng 1 phút",
        minutes: "%d phút",
        hour: "khoảng 1 giờ",
        hours: "khoảng %d giờ",
        day: "1 ngày",
        days: "%d ngày",
        month: "khoảng 1 tháng",
        months: "%d tháng",
        year: "khoảng 1 năm",
        years: "%d năm"
        //prefix: "",
        //suffix: " ago",
        //seconds: "less than a minute",
        //minute: "about a minute",
        //minutes: "%d minutes",
        //hour: "about an hour",
        //hours: "about %d hours",
        //day: "a day",
        //days: "%d days",
        //month: "about a month",
        //months: "%d months",
        //year: "about a year",
        //years: "%d years"
    };
    var template = function (t, n) {
        return templates[t] && templates[t].replace(/%d/i, Math.abs(Math.round(n)));
    };

    var timer = function (time) {
        if (!time) return;
        time = time.replace(/\.\d+/, ""); // remove milliseconds
        time = time.replace(/-/, "/").replace(/-/, "/");
        time = time.replace(/T/, " ").replace(/Z/, " UTC");
        time = time.replace(/([\+\-]\d\d)\:?(\d\d)/, " $1$2"); // -04:00 -> -0400
        time = new Date(time * 1000 || time);

        var now = new Date();
        var seconds = ((now.getTime() - time) * .001) >> 0;
        var minutes = seconds / 60;
        var hours = minutes / 60;
        var days = hours / 24;
        var years = days / 365;

        return templates.prefix + (
            seconds < 45 && template('seconds', seconds) || seconds < 90 && template('minute', 1) || minutes < 45 && template('minutes', minutes) || minutes < 90 && template('hour', 1) || hours < 24 && template('hours', hours) || hours < 42 && template('day', 1) || days < 30 && template('days', days) || days < 45 && template('month', 1) || days < 365 && template('months', days / 30) || years < 1.5 && template('year', 1) || template('years', years)) + templates.suffix;
    };

    var elements = document.getElementsByClassName('format-time');
    for (var i in elements) {
        var $this = elements[i];
        if (typeof $this === 'object') {
            $this.innerHTML = timer($this.getAttribute('title') || $this.getAttribute('datetime'));
        }
    }
    // update time every minute
    setTimeout(timeAgo, 30000);
})();

var Seanato = function () {
    const _errorCode = {
        ErrorList: -1,
        ErrorMess: -2,
        Success: 1
    };

    // ui helper
    var replaceCommonData = function () {
    }

    var preventDoubleSubmission = function () {
        // jQuery plugin to prevent double submission of forms
        jQuery.fn.preventDoubleSubmission = function () {
            $(this).on('submit', function (e) {
                var $form = $(this);

                if ($form.data('submitted') === true) {
                    // Previously submitted - don't submit again
                    //alert('Form already submitted. Please wait.');
                    e.preventDefault();
                } else {
                    // Mark it so that the next submit can be ignored
                    // ADDED requirement that form be valid
                    if (typeof $form.valid !== 'undefined' && $form.valid()) {
                        $form.data('submitted', true);
                    }
                }
            });

            // Keep chainability
            return this;
        };

        $('form').preventDoubleSubmission();
    }

    var uiHelperToastr = function () {
        // toastr ======================================================//
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        // end toastr ==================================================//
    }

    var uiFilterHtml = function (inputElement, filterElement) {
        $(inputElement).on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $(filterElement).filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    }

    // core
    var _loadDataAsync = function () {
        $(".asyncPartial").each(function (i, item) {
            var url = $(item).data("url");
            if (url && url.length > 0) {
                $(item).load(url);
            }
        });
    }

    function _addEventConfirmRedirectLink() {
        $("#page-top").on("click", ".btn-confirm", function (e) {
            e.preventDefault();

            if (confirm($(this).attr("data-title"))) {
                $.ajax({
                    url: $(this).attr("data-href"),
                    method: 'GET',
                    success: function (res) {
                        window.location.reload();
                    }
                });
            }
        });
    }

    function _addEventShowHiddenData() {
        $("#page-top").on("click", 'a[data-toggle="hidden"]', function (e) {
            e.preventDefault();
            window.prompt("Password", $(this).attr("data-hidden"));
            return false;
        });
    }

    return {
        init: function () {
            // ui data
            replaceCommonData();

            // core
            _loadDataAsync();

            // ui helper
            _addEventConfirmRedirectLink();
            _addEventShowHiddenData();

            preventDoubleSubmission();
            uiHelperToastr();
        },
        filterHtml: function (inputElement, filterElement) {
            uiFilterHtml(inputElement, filterElement);
        },
        errorCode: _errorCode,
        helper: function (helper) {
            switch (helper) {
                case 'loadDataAsync':
                    _loadDataAsync();
                    break;
                default:
                    return false;
            }
        },
        helpers: function (helpers) {
            if (helpers instanceof Array) {
                for (var index in helpers) {
                    Seanato.helper(helpers[index]);
                }
            } else {
                Seanato.helper(helpers);
            }
        }
    };
}();

$(function () {
    // init page
    Seanato.init();
    initInputGroupNumber();
});

function initInputGroupNumber() {
    $('input.input-group-number').each(function (i, obj) {
        var $this = $(obj);
        // format number
        $this.parent().find(".input-group-text").html(
            $this.val()
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        );
    });
    $("input.input-group-number").keyup(function (event) {
        var $this = $(this);

        // skip for arrow keys
        if (event.which >= 37 && event.which <= 40) return;

        // format number
        $this.parent().find(".input-group-text").html(
            $this.val()
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        );
    });
}

$("form #CompanyId").on('change', function () {
    var $form = $(this).parent().closest('form');
    if ($form.find("#CompanyLocationId").length) {
        $.ajax({
            method: "POST",
            url: '/Companies/GetListLocationByCompanyId',
            data: { companyId: this.value },
            success: function (res) {
                $("form #CompanyLocationId").html('<option value="">--- Địa điểm ---</option>');

                $.each(res, function (i, v) {
                    $("form #CompanyLocationId").append('<option value="' + v.Id + '">' + v.Name + '</option>');
                });
            }, error: function (error) {
                alert("Error!");
            }
        });
    }
});

/// ------------------ search modal user
var $frmSearchUserModal = $("#formSearchUserModal");
$frmSearchUserModal.submit(function (ev) {
    ev.preventDefault();

    $.ajax({
        method: $frmSearchUserModal.attr("method"),
        url: $frmSearchUserModal.attr("action"),
        data: $frmSearchUserModal.serializeArray(),
        success: function (res) {
            $("#dataSearchUserModal").html(res);

            registerEventSearchUser();
        }
    });
});

function registerEventSearchUser() {
    $(".pagination > li > a").each(function () {
        var url = $(this).attr("href");
        if (url !== undefined && url.length > 0) {
            var pNum = url.substring(url.lastIndexOf("=") + 1);
            $(this).attr("data-page", pNum);
        }
    });

    $(".pagination > li > a").click(function (e) {
        e.preventDefault();
        page = $(this).attr("data-page");

        $frmSearchUserModal.find('input[name="page"]').val(page);
        $frmSearchUserModal.submit();
    });
}

$("#dataSearchUserModal").on("click", ".btn-select-single-user", function (e) {
    e.preventDefault();

    var $btn = $(this);
    $("#frmVoucher").find('input[name="UserId"]').val($btn.attr("data-id"));
    $("#frmVoucher").find('input[name="UserInfo"]').val($btn.attr("data-info"));

    $("#modalSearchSelectUser").modal("hide");
});
/// ------------------ end search modal user


//$("#profile-img").click(function () {
//    $("#status-options").toggleClass("active");
//});

//$(".expand-button").click(function () {
//    $("#profile").toggleClass("expanded");
//    $("#contacts").toggleClass("expanded");
//});

//$("#status-options ul li").click(function () {
//    $("#profile-img").removeClass();
//    $("#status-online").removeClass("active");
//    $("#status-away").removeClass("active");
//    $("#status-busy").removeClass("active");
//    $("#status-offline").removeClass("active");
//    $(this).addClass("active");

//    if ($("#status-online").hasClass("active")) {
//        $("#profile-img").addClass("online");
//    } else if ($("#status-away").hasClass("active")) {
//        $("#profile-img").addClass("away");
//    } else if ($("#status-busy").hasClass("active")) {
//        $("#profile-img").addClass("busy");
//    } else if ($("#status-offline").hasClass("active")) {
//        $("#profile-img").addClass("offline");
//    } else {
//        $("#profile-img").removeClass();
//    };

//    $("#status-options").removeClass("active");
//});

//function newMessage() {
//    message = $(".message-input input").val();
//    if ($.trim(message) == '') {
//        return false;
//    }
//    $('<li class="sent"><img src="http://emilcarlsson.se/assets/mikeross.png" alt="" /><p>' + message + '</p></li>').appendTo($('.messages ul'));
//    $('.message-input input').val(null);
//    $('.contact.active .preview').html('<span>You: </span>' + message);
//    $(".messages").animate({ scrollTop: $(document).height() }, "fast");
//};

//$('.submit').click(function () {
//    newMessage();
//});

//$(window).on('keydown', function (e) {
//    if (e.which == 13) {
//        newMessage();
//        return false;
//    }
//});