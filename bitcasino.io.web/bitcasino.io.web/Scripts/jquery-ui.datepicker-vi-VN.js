﻿jQuery(function ($) {
    $.datepicker.regional["vi-VN"] =
      {
          closeText: "Đóng",
          prevText: "Trước",
          nextText: "Sau",
          currentText: "Hôm nay",
          monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
          monthNamesShort: ["Một", "Hai", "Ba", "Bốn", "Năm", "Sáu", "Bảy", "Tám", "Chín", "Mười", "Mười một", "Mười hai"],
          dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
          dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
          dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
          weekHeader: "Tuần",
          dateFormat: "dd/mm/yy",
          firstDay: 1,
          isRTL: false,
          showMonthAfterYear: false,
          yearSuffix: "",
          changeMonth: true,
          changeYear: true,
          yearRange: '1920:2030',// + new Date().getFullYear(),
          defaultDate: new Date(),
          onChangeMonthYear: function (y, m, i) {
              var d = i.selectedDay;
              $(this).datepicker('setDate', new Date(y, m - 1, d));
          }
      };

    $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);

    $.validator.methods.date = function (value, element) {
        if (value) {
            try {
                $.datepicker.parseDate('dd/mm/yy', value);
            } catch (ex) {
                return false;
            }
        }
        return true;
    };
});