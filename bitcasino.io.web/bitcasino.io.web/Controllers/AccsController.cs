﻿using bitcasino.io.web.Helpers;
using bitcasino.io.web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using X.PagedList;

namespace bitcasino.io.web.Controllers
{
    public class AccsController : Controller
    {
        private bitcasinoEntities db = new bitcasinoEntities();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Accs
        public ActionResult Index(string username, string password, bool? isUsing, int? page = 1)
        {
            int pageSize = 20;
            page = page > 0 ? page : 1;

            IQueryable<AccsModel> model = (from a in db.Accs select new AccsModel
            {
                Id = a.Id,
                Email = a.Email,
                PasswordRaw = a.PasswordRaw,
                IsUsing = a.IsUsing
            });
            
            var result = model.OrderBy(o => o.Id).ToPagedList(page.Value, pageSize);

            ViewBag.PageList = model;
            return View(result);
        }
        // GET: Accs/Create
        public ActionResult Create()
        {
            var TrangThai = new List<object>() { new { name = "Đang hoạt động", value = true }, new { name = "Chưa hoạt động", value = false } };
            ViewBag.TrangThaiId = new SelectList(TrangThai, "value", "name");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateAccsBindingModel model)
        {
            if (ModelState.IsValid)
            {
                var accs = db.Accs.Where((p) => p.Email == model.Email).FirstOrDefault();
                if (accs != null)
                {
                    accs.Email = model.Email;
                    accs.PasswordRaw = model.PasswordRaw;
                    accs.IsUsing = model.IsUsing.Equals("True") ? true : false;
                }
                else
                {
                    Acc acc = new Acc();
                    acc.Email = model.Email;
                    acc.PasswordRaw = model.PasswordRaw;
                    acc.IsUsing = model.IsUsing.Equals("True") ? true : false;
                    db.Accs.Add(acc);
                }
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ee)
                {

                    throw;
                }
                return RedirectToAction("Index", "Accs");
            }

            return View(model);
        }
        public ActionResult EditAcc(int id)
        {
            var TrangThai = new List<object>() { new { name = "Đang hoạt động", value = true }, new { name = "Chưa hoạt động", value = false } };
            ViewBag.TrangThaiId = new SelectList(TrangThai, "value", "name");
            var acc = db.Accs.Find(id);
            if (acc == null)
            {
                return HttpNotFound();
            }

            var model = new CreateAccsBindingModel();
            model.Id = acc.Id;
            model.Email = acc.Email;
            model.PasswordRaw = acc.PasswordRaw;
            model.IsUsing = acc.IsUsing;

            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        public async Task<ActionResult> EditAcc(CreateAccsBindingModel model)
        {
            if (ModelState.IsValid)
            {
                var acc = db.Accs.Find(model.Id);
                if (acc == null)
                {
                    return HttpNotFound();
                }
                
                acc.Email = model.Email;
                acc.PasswordRaw = model.PasswordRaw;
                acc.IsUsing = model.IsUsing.Value;

                await db.SaveChangesAsync();
                return RedirectToAction("Index", "Accs");
            }
            return View(model);
        }

        public ActionResult DeleteAcc(int id)
        {
            var acc = db.Accs.Find(id);
            if (acc == null)
            {
                return HttpNotFound();
            }
            var model = new DeleteAccViewModel();
            model.AccId = id;

            return PartialView("_DeleteAcc", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        public ActionResult DeleteAcc(DeleteAccViewModel model)
        {
            List<ResultError> errors = new List<ResultError>();
            if (!ModelState.IsValid)
            {
                ResultError.GetListErrors(ModelState, ref errors);
                return Json(new { code = ErrorCode.ErrorList, errs = errors });
            }

            var uId = User.Identity.GetUserId();
            var checkUser = UserManager.FindById(uId);
            if (UserManager.PasswordHasher.VerifyHashedPassword(checkUser.PasswordHash, model.Password) == PasswordVerificationResult.Failed)
            {
                ModelState.AddModelError("Password", "Wrong password!");
                ResultError.GetListErrors(ModelState, ref errors);
                return Json(new { code = ErrorCode.ErrorList, errs = errors });
            }
            var acc = db.Accs.Find(model.AccId);
            if (acc == null)
            {
                ModelState.AddModelError("Accs", "Not found!");
                ResultError.GetListErrors(ModelState, ref errors);
                return Json(new { code = ErrorCode.ErrorList, errs = errors });
            }

            db.Accs.Remove(acc);
            //db.Database.ExecuteSqlCommand("UPDATE dbo.[Order] SET ApproveUserId = NULL WHERE ApproveUserId = @id", new SqlParameter("id", user.Id));
            db.SaveChanges();

            return Json(new { code = ErrorCode.Success });
        }
    }
}