﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace bitcasino.io.web.Controllers
{
    [Authorize(Roles = "Admin, Manager")]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}