﻿using bitcasino.io.web.Helpers;
using bitcasino.io.web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using X.PagedList;

namespace bitcasino.io.web.Controllers
{
    [Authorize(Roles = "Admin,Manager")]
    public class UsersController : Controller
    {
        #region Helpers
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        #endregion
        #region PROPERTIES
        private bitcasinoEntities db = new bitcasinoEntities();

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        #endregion
        // GET: Users/Create
        public ActionResult Create()
        {
            ViewBag.RoleId = new SelectList(db.AspNetRoles, "Id", "Name");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        public async Task<ActionResult> Create(CreateUserBindingModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, PhoneNumber = model.Phone };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var userCreated = db.AspNetUsers.Where(p => p.Email == model.Email && p.PhoneNumber == model.Phone && p.UserName == model.Email).FirstOrDefault();
                    if (userCreated != null)
                    {
                        UserInfo userInfo = new UserInfo();
                        userInfo.UserId = userCreated.Id;
                        userInfo.ActiveDate = new DateTime(model.ActiveDate.Year, model.ActiveDate.Month, model.ActiveDate.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                        userInfo.Duration = model.Duration;
                        userInfo.PasswordUser = model.Password;
                        db.UserInfoes.Add(userInfo);

                        string userRole = (model.RoleId == "1") ? "Admin" : (model.RoleId == "2") ? "Manager" : "User";
                        await this.UserManager.AddToRoleAsync(userCreated.Id, userRole);

                        await db.SaveChangesAsync();
                        return RedirectToAction("Index", "Users");
                    }
                }
                AddErrors(result);
            }

            ViewBag.RoleId = new SelectList(db.AspNetRoles, "Id", "Name");
            return View(model);
        }

        public ActionResult EditUser(string id)
        {
            ViewBag.RolesId = new SelectList(db.AspNetRoles, "Id", "Name");
            var user = UserManager.FindById(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            var userInfo = db.UserInfoes.Find(id);
            if (userInfo == null)
            {
                return HttpNotFound();
            }

            var model = new CreateUserBindingModel();
            model.Id = id;
            model.Email = user.Email;
            model.Phone = user.PhoneNumber;
            model.RoleId = user.Roles.FirstOrDefault().RoleId;
            model.Duration = userInfo.Duration;
            model.Password = userInfo.PasswordUser;
            model.ActiveDate = userInfo.ActiveDate;

            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        public async Task<ActionResult> EditUser(CreateUserBindingModel model)
        {
            if (ModelState.IsValid)
            {
                var user = UserManager.FindById(model.Id);
                if (user == null)
                {
                    return HttpNotFound();
                }

                var userInfo = db.UserInfoes.Find(model.Id);
                if (userInfo == null)
                {
                    return HttpNotFound();
                }

                user.Email = model.Email;
                user.UserName = model.Email;
                user.PhoneNumber = model.Phone;
                userInfo.Duration = model.Duration;
                userInfo.PasswordUser = model.Password;
                userInfo.ActiveDate = model.ActiveDate;
                string userRole = (model.RoleId == "1") ? "Admin" : (model.RoleId == "2") ? "Manager" : "User";
                await this.UserManager.RemoveFromRoleAsync(user.Id, (await UserManager.GetRolesAsync(user.Id)).FirstOrDefault());
                await this.UserManager.AddToRoleAsync(user.Id, userRole);

                try
                {
                    var token = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    var result = await UserManager.ResetPasswordAsync(user.Id, token, model.Password);
                }
                catch (Exception ee)
                {
                }

                await db.SaveChangesAsync();
                return RedirectToAction("Index", "Users");
            }
            //else
            //{
            //    List<ResultError> errors = new List<ResultError>();
            //    ResultError.GetListErrors(ModelState, ref errors);
            //    return Json(new { code = ErrorCode.ErrorList, errs = errors });
            //}
            ViewBag.RolesId = new SelectList(db.AspNetRoles, "Id", "Name");
            return View(model);
        }


        public ActionResult DeleteUser(string id)
        {
            var user = db.UserInfoes.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            var model = new DeleteUserViewModel();
            model.UserId = id;

            return PartialView("_DeleteUser", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(true)]
        public ActionResult DeleteUser(DeleteUserViewModel model)
        {
            List<ResultError> errors = new List<ResultError>();
            if (!ModelState.IsValid)
            {
                ResultError.GetListErrors(ModelState, ref errors);
                return Json(new { code = ErrorCode.ErrorList, errs = errors });
            }

            var uId = User.Identity.GetUserId();
            var checkUser = UserManager.FindById(uId);
            if (UserManager.PasswordHasher.VerifyHashedPassword(checkUser.PasswordHash, model.Password) == PasswordVerificationResult.Failed)
            {
                ModelState.AddModelError("Password", "Wrong password!");
                ResultError.GetListErrors(ModelState, ref errors);
                return Json(new { code = ErrorCode.ErrorList, errs = errors });
            }

            var appUser = UserManager.FindById(model.UserId);
            UserManager.Delete(appUser);

            var user = db.UserInfoes.Find(model.UserId);
            if (user == null)
            {
                ModelState.AddModelError("Password", "Not found!");
                ResultError.GetListErrors(ModelState, ref errors);
                return Json(new { code = ErrorCode.ErrorList, errs = errors });
            }

            db.UserInfoes.Remove(user);
            
            //db.Database.ExecuteSqlCommand("UPDATE dbo.[Order] SET ApproveUserId = NULL WHERE ApproveUserId = @id", new SqlParameter("id", user.Id));
            db.SaveChanges();

            return Json(new { code = ErrorCode.Success });
        }

        public ActionResult Index(string username, string email, string phonenumber, int? expiredays, string tab, bool? isOnline, int? page = 1, bool? download = false)
        {
            int pageSize = 20;
            page = page > 0 ? page : 1;

            IQueryable<Proc_Admin_GetListUser_Result> model = null;
            if (string.IsNullOrEmpty(tab))
            {
                model = (from u in db.AspNetUsers
                 join ui in db.UserInfoes on u.Id equals ui.UserId
                 
                 select new Proc_Admin_GetListUser_Result
                 {
                     Id = u.Id,
                     ActiveDate = ui.ActiveDate,
                     Duration = ui.Duration,
                     Email = u.Email,
                     PhoneNumber = u.PhoneNumber,
                     PasswordUser = ui.PasswordUser,
                     Role = u.AspNetRoles.FirstOrDefault(),
                     UserName = u.UserName,
                     IsOnline = ui.IsOnline
                 });
            }
            else
            {
                model = (from u in db.AspNetUsers
                 join ui in db.UserInfoes on u.Id equals ui.UserId
                 where u.AspNetRoles.Any(x => x.Name == tab)
                 select new Proc_Admin_GetListUser_Result
                 {
                     Id = u.Id,
                     ActiveDate = ui.ActiveDate,
                     Duration = ui.Duration,
                     Email = u.Email,
                     PhoneNumber = u.PhoneNumber,
                     PasswordUser = ui.PasswordUser,
                     Role = u.AspNetRoles.FirstOrDefault(),
                     UserName = u.UserName,
                     IsOnline = ui.IsOnline
                 });
            }
            //model = (expiredays == null) ? model : model.Where((p) => expiredays.Value == (EntityFunctions.AddDays(p.ActiveDate.Date, p.Duration).Value - DateTime.Now.Date).Days);
            model = (expiredays == null) ? model : model.Where((p) => expiredays.Value == DbFunctions.DiffDays(DateTime.Now, DbFunctions.AddDays(p.ActiveDate, p.Duration)).Value);
            model = (string.IsNullOrEmpty(email)) ? model : model.Where((p) => p.Email == email);
            model = (string.IsNullOrEmpty(phonenumber)) ? model : model.Where((p) => p.PhoneNumber == phonenumber);
            model = (string.IsNullOrEmpty(username)) ? model : model.Where((p) => p.UserName == username);
            model = (isOnline == null) ? model : model.Where((p) => p.IsOnline == isOnline);

            var result = model.OrderBy(o => o.ActiveDate).ToPagedList(page.Value, pageSize);

            ViewBag.PageList = model;
            var TrangThaiOnline = new List<bool> { true, false };
            ViewBag.TrangThaiOnline = new SelectList(TrangThaiOnline, "", "");
            return View(result);
        }

        //[Authorize(Roles = "Admin")]
        //public ActionResult EditUserRole(string id)
        //{
        //    var roleStore = new RoleStore<IdentityRole>();
        //    var roleMngr = new RoleManager<IdentityRole>(roleStore);

        //    ViewBag.Roles = roleMngr.Roles.Select(x => x.Name).ToList();

        //    var model = new EditUserRoleViewModel();
        //    model.UserId = id;
        //    model.Roles = UserManager.GetRoles(id).ToList();
        //    return PartialView("_EditUserRole", model);
        //}

        //[Authorize(Roles = "Admin")]
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult EditUserRole(EditUserRoleViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var roles = UserManager.GetRoles(model.UserId);
        //        UserManager.RemoveFromRoles(model.UserId, roles.ToArray());

        //        if (model.Roles != null)
        //        {
        //            UserManager.AddToRoles(model.UserId, model.Roles.ToArray());
        //        }
        //        return Json(true);
        //    }
        //    return Json(false);
        //}

        //public ActionResult LockUser(string id)
        //{
        //    //await UserManager.SetLockoutEnabledAsync(id, true);
        //    //await UserManager.SetLockoutEndDateAsync(id, DateTime.Now.AddYears(100));

        //    var user = UserManager.FindById(id);
        //    if (!User.IsInRole("Admin"))
        //    {
        //        if (user.Roles.Any(x => x.RoleId == "bc64a68f-fd0c-447c-b624-c55971d2d06f")) // admin
        //        {
        //            return Json(false, JsonRequestBehavior.AllowGet);
        //        }
        //    }

        //    user.LockoutEnabled = true;
        //    if (user.LockoutEndDateUtc != null)
        //    {
        //        user.LockoutEndDateUtc = null;
        //    }
        //    else
        //    {
        //        user.LockoutEndDateUtc = DateTime.Now.AddYears(200);
        //    }
        //    UserManager.Update(user);

        //    return Json(true, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult Confirm(string id)
        //{
        //    var user = UserManager.FindById(id);
        //    user.EmailConfirmed = true;
        //    UserManager.Update(user);

        //    return Json(true, JsonRequestBehavior.AllowGet);
        //}
        

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteUser(DeleteUserViewModel model)
        //{
        //    List<ResultError> errors = new List<ResultError>();
        //    if (!ModelState.IsValid)
        //    {
        //        ResultError.GetListErrors(ModelState, ref errors);
        //        return Json(new { code = ErrorCode.ErrorList, errs = errors });
        //    }

        //    var uId = User.Identity.GetUserId();
        //    var checkUser = UserManager.FindById(uId);
        //    if (UserManager.PasswordHasher.VerifyHashedPassword(checkUser.PasswordHash, model.Password) == PasswordVerificationResult.Failed)
        //    {
        //        ModelState.AddModelError("Password", "Wrong password!");
        //        ResultError.GetListErrors(ModelState, ref errors);
        //        return Json(new { code = ErrorCode.ErrorList, errs = errors });
        //    }

        //    var appUser = UserManager.FindById(model.UserId);
        //    UserManager.Delete(appUser);

        //    var user = db.UserDetails.Find(model.UserId);
        //    if (user == null)
        //    {
        //        ModelState.AddModelError("Password", "Not found!");
        //        ResultError.GetListErrors(ModelState, ref errors);
        //        return Json(new { code = ErrorCode.ErrorList, errs = errors });
        //    }

        //    db.UserDetails.Remove(user);
        //    //db.Database.ExecuteSqlCommand("UPDATE dbo.[Order] SET ApproveUserId = NULL WHERE ApproveUserId = @id", new SqlParameter("id", user.Id));
        //    db.SaveChanges();

        //    return Json(new { code = ErrorCode.Success });
        //}

        //[HttpPost]
        //public ActionResult CheckExistingPhone(string phoneNumber)
        //{
        //    try
        //    {
        //        return Json(IsPhoneExists(phoneNumber));
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(false);
        //    }
        //}

        //[HttpPost]
        //public ActionResult CheckExistingEmail(string email)
        //{
        //    try
        //    {
        //        return Json(!IsEmailExists(email));
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(false);
        //    }
        //}

        //private bool IsEmailExists(string email)
        //    => UserManager.FindByEmail(email) != null;

        //private bool IsPhoneExists(string phone)
        //    => UserManager.Users.Any(x => x.PhoneNumber == phone);

        //public ActionResult SearchUserModal(string keyword, int page = 1)
        //{
        //    int pageSize = 10;
        //    int pageNumber = page < 1 ? 1 : page;

        //    var model = db.AspNetUsers.Where(x => x.UserName.Contains(keyword) || x.PhoneNumber.Contains(keyword)).OrderByDescending(x => x.Id).ToPagedList(pageNumber, pageSize);

        //    return PartialView("_SearchAndSelectUserData", model);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}