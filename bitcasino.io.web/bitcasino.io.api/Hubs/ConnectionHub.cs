﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Web;
using bitcasino.io.api;
using bitcasino.io.api.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin;

[assembly: OwinStartupAttribute(typeof(Startup))]
namespace bitcasino.io.api.Hubs
{
    [HubName("ConnectionHub")]
    public class ConnectionHub : Hub
    {
        //System.Timers.Timer timer = new System.Timers.Timer(1000); // 1000 = 1 second
        public override Task OnConnected()
        {
            try
            {
                //WriteLog("vao thanh cong ne");
                if (Context.User.Identity.IsAuthenticated)
                {
                    var id = Context.User.Identity.GetUserId();
                    using (bitcasinoEntities db = new bitcasinoEntities())
                    {
                        var model = new ConnectionSignalR();
                        model.ConnectionId = Context.ConnectionId;
                        model.UserId = id;
                        model.CreateTime = DateTime.Now;
                        db.ConnectionSignalRs.Add(model);

                        var userInfo = db.UserInfoes.Where(o => o.UserId == id).FirstOrDefault();
                        if (userInfo != null)
                        {
                            userInfo.IsOnline = true;
                        }
                        db.SaveChanges();
                    }
                }
                return base.OnConnected();
            }
            catch (Exception e)
            {
                var mes = e.ToString();
                //WriteLog(mes);
                return base.OnConnected();
            }

        }

        public void SendPing()
        {
            using (bitcasinoEntities db = new bitcasinoEntities())
            {
                var userId = Context.User.Identity.GetUserId();
                var model = db.ConnectionSignalRs.Where(x => x.UserId == userId).Select(x => x.ConnectionId).ToList();
                if (model.Count > 0)
                {
                    var userInfo = db.UserInfoes.Where(o => o.UserId == userId).FirstOrDefault();
                    try
                    {
                        IHubContext _hubContext = GlobalHost.ConnectionManager.GetHubContext<ConnectionHub>();
                        _hubContext.Clients.Clients(model).ReturnPing((userInfo.ActiveDate.AddDays(userInfo.Duration.Value) - DateTime.Now).Days,
                            (userInfo.ActiveDate.AddDays(userInfo.Duration.Value) - DateTime.Now).Hours,
                            (userInfo.ActiveDate.AddDays(userInfo.Duration.Value) - DateTime.Now).Minutes,
                            (userInfo.ActiveDate.AddDays(userInfo.Duration.Value) - DateTime.Now).Seconds);
                    }
                    catch (Exception ee)
                    {
                        
                    }
                }
            }
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            if (Context.User.Identity.IsAuthenticated)
            {
                var id = Context.User.Identity.GetUserId();
                using (bitcasinoEntities db = new bitcasinoEntities())
                {
                    var model = db.ConnectionSignalRs.Where(x => x.ConnectionId == Context.ConnectionId).FirstOrDefault();
                    if (model != null)
                    {
                        db.ConnectionSignalRs.Remove(model);
                    }
                    var userInfo = db.UserInfoes.Where(o => o.UserId == id).FirstOrDefault();
                    if (userInfo != null)
                    {
                        userInfo.IsOnline = false;
                    }
                    db.SaveChanges();
                }
            }
            return base.OnDisconnected(stopCalled);
        }
    }
}