﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bitcasino.io.api.Models
{
    public class UserAccountInfo
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string PasswordUser { get; set; }
        public byte? Duration { get; set; }
        public DateTime ActiveDate { get; set; }
    }
}