﻿using bitcasino.io.api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;


namespace bitcasino.io.api.Controllers
{
    [Authorize]
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        private bitcasinoEntities db = new bitcasinoEntities();

        // GET api/User/GetUserInfo
        [Route("GetUserInfo")]
        public IHttpActionResult GetUserInfo()
        {
            var result = new UserAccountInfo();
            string uid = User.Identity.GetUserId();
            var model = (from u in db.AspNetUsers
                     join ui in db.UserInfoes on u.Id equals ui.UserId

                     select new UserAccountInfo
                     {
                         Id = u.Id,
                         ActiveDate = ui.ActiveDate,
                         Duration = ui.Duration,
                         Email = u.Email,
                         PhoneNumber = u.PhoneNumber,
                         PasswordUser = ui.PasswordUser,
                         UserName = u.UserName
                     }).Where((p) => p.Id == uid).FirstOrDefault();
            if (model != null)
            {
                result.Id = model?.Id;
                result.ActiveDate = model.ActiveDate;
                result.Duration = model.Duration;
                result.Email = model?.Email;
                result.PhoneNumber = model.PhoneNumber;
                result.PasswordUser = model?.PasswordUser;
                result.UserName = model?.UserName;
                return Ok(result);
            }
            return NotFound();
        }

        // GET api/User/PingUser
        [Route("PingUser")]
        [HttpPost]
        public IHttpActionResult PingUser()
        {
            string uid = User.Identity.GetUserId();
            return Ok(CountDown(uid));
        }

        private int CountDown(string userId)
        {
            var userInfo = db.UserInfoes.Where(o => o.UserId == userId).FirstOrDefault();
            return (userInfo.ActiveDate.AddDays(userInfo.Duration.Value) - DateTime.Now).Seconds;
        }
    }
}
