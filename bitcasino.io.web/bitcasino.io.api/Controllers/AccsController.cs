﻿using bitcasino.io.api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace bitcasino.io.api.Controllers
{
    [Authorize]
    [RoutePrefix("api/Accs")]
    public class AccsController : ApiController
    {
        private bitcasinoEntities db = new bitcasinoEntities();

        // GET api/Accs/GetAccs
        [Route("GetAccs")]
        public async Task<IHttpActionResult> GetAccs()
        {
            var result = new Accs();
            var data = db.Accs.FirstOrDefault((p) => p.IsUsing == false);
            if (data != null)
            {
                result.Id = data.Id;
                result.Email = data?.Email;
                result.PasswordRaw = data?.PasswordRaw;
                //data.IsUsing = true;
                await db.SaveChangesAsync();
                return Ok(result);
            }
            return NotFound();
        }
    }
}
